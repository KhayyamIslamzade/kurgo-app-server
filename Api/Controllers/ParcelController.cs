﻿using Application.Middlewares;
using Application.Services;
using AutoMapper;
using AutoWrapper.Extensions;
using Core.Enums;
using Core.Extensions;
using Core.Utilities;
using Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Process;
using Process.Repository;
using Shared;
using Shared.Resources.Parcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ParcelController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Parcel> _repository;
        private readonly IRepository<ParcelOption> _parcelOptionRepository;
        private readonly UserService _userService;
        private readonly IMapper _mapper;
        private readonly string[] _includes = new string[] { "Category", "Seller", "Currency", "ParcelStatuses", "ParcelOption.Currency" };
        public ParcelController(IUnitOfWork unitOfWork, IRepository<Parcel> repository, IRepository<ParcelOption> parcelOptionRepository, UserService userService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _parcelOptionRepository = parcelOptionRepository;
            _userService = userService;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public async Task<ActionResponse> Get(int id)
        {
            var userId = _userService.GetAuthorizedUserId();
            var hasPermission = await _userService.UserIsInPermissionAsync(userId, "parcel_list");
            Parcel item = null;
            if (hasPermission)
            {
                List<string> includes = new List<string>();
                includes.AddRange(_includes);
                includes.Add("User.Detail");
                item = await _repository.GetSingleFirstAsync(c => c.Id == id, _includes).ConfigureAwait(false);
            }
            else
                item = await _repository.GetSingleFirstAsync(c => c.Id == id && c.UserId == userId, _includes).ConfigureAwait(false);


            if (item != null)
                return new ActionResponse(_mapper.Map<Parcel, ParcelGetData>(item), StatusCodes.Status200OK);
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

        }
        [HttpGet("GetAll")]
        public async Task<ActionResponse> GetAll([FromQuery] ParcelFilterParameters filterParameters, [FromQuery] PagingParameters pagingParameters)
        {
            var userId = _userService.GetAuthorizedUserId();
            var hasPermission = await _userService.UserIsInPermissionAsync(userId, "parcel_list");

            IEnumerable<Parcel> data;
            if (hasPermission)
            {
                List<string> includes = new List<string>();
                includes.AddRange(_includes);
                includes.Add("User.Detail");
                data = _repository.GetAll(includes.ToArray());

                var filter = filterParameters;

                if (filter != null)
                {

                    var isDateExist = filter.DateRange != null && filter.DateRange.Any() && filter.DateRange.Length == 2;
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;
                    if (isDateExist)
                    {
                        startDate = filter.DateRange[0].ToCustomDateTime();
                        endDate = filter.DateRange[1].ToCustomDateTime();

                    }

                    var isTrackingNumberExist = !string.IsNullOrEmpty(filter.TrackingNumber);
                    var isPhoneNumberExist = !string.IsNullOrEmpty(filter.PhoneNumber);
                    var isFinNumberExist = !string.IsNullOrEmpty(filter.FinNumber);
                    var isIdentificationNumberExist = !string.IsNullOrEmpty(filter.IdentificationNumber);
                    var isUserExist = !string.IsNullOrEmpty(filter.User);
                    var isStatusExist = filter.Status != null && filter.Status != 0;
                    data = data.Where(c =>
                        (!isTrackingNumberExist || c.TrackingNumber.ToLower().Contains(filter.TrackingNumber.ToLower())) &&
                        (!isPhoneNumberExist || c.User.Detail != null && c.User.Detail.PhoneNumber.ToLower().Contains(filter.PhoneNumber.ToLower())) &&
                        (!isFinNumberExist || c.User.Detail != null && c.User.Detail.FinNumber.ToLower().Contains(filter.FinNumber.ToLower())) &&
                        (!isIdentificationNumberExist || c.User.Detail != null && c.User.Detail.IdentificationNumber.ToLower().Contains(filter.IdentificationNumber.ToLower())) &&


                        (!isStatusExist || (c.ParcelStatus != null && (byte)c.ParcelStatus.StatusOfParcel == (byte)filter.Status))
                        &&
                        (!isUserExist || ((c.User.UserName.ToLower().Contains(filter.User.ToLower())) || (c.User.Detail != null && (c.User.Detail.Name.ToLower().Contains(filter.User.ToLower()) || c.User.Detail.Surname.ToLower().Contains(filter.User.ToLower())))))
                        && (!isDateExist || (c.DateCreated.Date >= startDate.Date && c.DateCreated.Date <= endDate.Date))
                        ).AsEnumerable();
                }

            }
            else
                data = _repository.FindBy(c => c.UserId == userId, _includes);




            var dataCount = data.Count();
            data = data.OrderByDescending(c => c.DateCreated);

            data = data.FindPaged(pagingParameters);

            var enumerable = data as Parcel[] ?? data.ToArray();
            foreach (var item in enumerable)
            {
                item.ParcelStatuses = item.ParcelStatuses.OrderByDescending(c => c.DateCreated).ToList();
            }

            var result = _mapper.Map<List<Parcel>, List<ParcelGetData>>(enumerable.ToList());
            return new ActionResponse(new
            {
                items = result,
                totalCount = dataCount
            }, StatusCodes.Status200OK);
        }

        [HttpPost("Add")]
        public async Task<ActionResponse> Add([FromBody] ParcelData data)
        {

            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);


            var userId = _userService.GetAuthorizedUserId();
            var user = await _userService.GetUserByIdAsync(userId);

            var hasPermission = await _userService.UserIsInPermissionAsync(userId, "parcel_list");

            if (!hasPermission && !user.IsDetailCompleted)
                return new ActionResponse(MessageBuilder.FirstCompleteDetail, StatusCodes.Status400BadRequest);


            var item = _mapper.Map<ParcelData, Parcel>(data);

            item.UserId = _userService.GetAuthorizedUserId();
            item.ParcelStatuses.Add(new ParcelStatus()
            {
                Status = (byte)ParcelStatusEnum.OnProcess,

            });

            await _repository.AddAsync(item).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await Get(item.Id).ConfigureAwait(false);
        }
        [Permission(policys: new string[] { "parcel_edit" })]
        [HttpPost("ChangeStatus")]
        public async Task<ActionResponse> ChangeStatus([FromBody] ParcelStatusData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.ParcelId, _includes).ConfigureAwait(false);
            if (item == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

            var parcelStatus = _mapper.Map<ParcelStatusData, ParcelStatus>(data);
            item.ParcelStatuses.Add(parcelStatus);

            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return new ActionResponse(StatusCodes.Status200OK);
        }



        [Permission(policys: new string[] { "parcel_edit" })]
        [HttpPost("SetOptions")]
        public async Task<ActionResponse> SetOptions([FromBody] ParcelOptionData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.ParcelId, _includes).ConfigureAwait(false);
            if (item == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

            var parcelOption = _mapper.Map<ParcelOptionData, ParcelOption>(data);
            item.ParcelOption = parcelOption;
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return new ActionResponse(StatusCodes.Status200OK);
        }
        [HttpPost("Edit")]
        public async Task<ActionResponse> Edit([FromBody] ParcelData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            var userId = _userService.GetAuthorizedUserId();
            var hasPermission = await _userService.UserIsInPermissionAsync(userId, "parcel_edit");
            Parcel item = null;

            if (hasPermission)
                item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id, _includes).ConfigureAwait(false);
            else
                item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id && c.UserId == userId, _includes).ConfigureAwait(false);

            if (!hasPermission && item.ParcelStatus != null && item.ParcelStatus.StatusOfParcel != (int)ParcelStatusEnum.OnProcess)
                return new ActionResponse(MessageBuilder.NotEditable, StatusCodes.Status400BadRequest);

            if (item == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

            _mapper.Map(data, item);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await Get(item.Id).ConfigureAwait(false);
        }
        [HttpPost("Delete/{id}")]
        public async Task<ActionResponse> Delete(int id)
        {
            var userId = _userService.GetAuthorizedUserId();
            var hasPermission = await _userService.UserIsInPermissionAsync(userId, "parcel_delete");

            Parcel item = null;
            if (hasPermission)
                item = await _repository.GetSingleFirstAsync(c => c.Id == id, "ParcelStatuses").ConfigureAwait(false);
            else
                item = await _repository.GetSingleFirstAsync(c => c.Id == id && c.UserId == userId, "ParcelStatuses").ConfigureAwait(false);

            if (!hasPermission && item.ParcelStatus != null && item.ParcelStatus.StatusOfParcel != (int)ParcelStatusEnum.OnProcess)
                return new ActionResponse(MessageBuilder.CantDeleteThisParcel, StatusCodes.Status404NotFound);

            if (item != null)
            {
                item.Status = (byte)RecordStatus.Deleted;
                await _unitOfWork.CompleteAsync().ConfigureAwait(false);
                return new ActionResponse(MessageBuilder.Deleted(), StatusCodes.Status200OK);
            }
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);
        }
    }

}
