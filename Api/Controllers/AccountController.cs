﻿using Application.Services;
using AutoMapper;
using AutoWrapper.Extensions;
using Core.Constants;
using Core.Extensions;
using Core.Utilities;
using Data.Entities;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Resources.User;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly UserService _userService;
        private readonly ConfirmationService _confirmationService;
        private readonly SignInManager<User> _signInManager;
        private readonly TokenService _tokenService;

        public AccountController(UserService userService, ConfirmationService confirmationService, SignInManager<User> signInManager, IMapper mapper,
            TokenService tokenService)
        {
            _userService = userService;
            _confirmationService = confirmationService;
            _signInManager = signInManager;
            _mapper = mapper;
            _tokenService = tokenService;
        }
        [HttpPost("Login")]
        public async Task<ActionResponse> Login([FromBody] UserLoginData data)
        {

            var isEmail = data.EmailOrUsername.IsEmail();
            var isUsername = data.EmailOrUsername.IsUsername();

            if (!(isEmail || isUsername))
                ModelState.AddModelError("emailOrUsername", "Enter valid Email/Username");

            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            var userQuery = _userService.GetAll(new IncludeStringConstants().UserRolePermissionIncludeArray);

            User user;
            switch (isUsername)
            {
                case true:
                    user = await userQuery.FirstOrDefaultAsync(c => c.UserName == data.EmailOrUsername)
                        .ConfigureAwait(false);
                    break;
                case false:
                    user = await userQuery.FirstOrDefaultAsync(c => c.Email == data.EmailOrUsername)
                        .ConfigureAwait(false);
                    break;
            }

            if (user != null)
            {
                if (!user.EmailConfirmed)
                    return new ActionResponse(MessageBuilder.ConfirmEmail, StatusCodes.Status400BadRequest);
                var result = await _signInManager.CheckPasswordSignInAsync(user, data.Password, false).ConfigureAwait(false);

                if (result.Succeeded)
                {
                    var mapResult = _mapper.Map<User, UserGetData>(user);

                    return new ActionResponse(new UserLoginResponse
                    {
                        User = mapResult,
                        Token = _tokenService.GenerateToken(user.Id)
                    });
                }
            }

            return new ActionResponse(MessageBuilder.LoginFault, StatusCodes.Status400BadRequest);
        }


        [HttpPost("Register")]
        public async Task<ActionResponse> Register([FromBody] UserRegisterData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());

            if (!await _userService.IsExistAsync(c => c.UserName == data.UserName || c.Email == data.Email)
                .ConfigureAwait(false))
            {
                var user = _mapper.Map<UserRegisterData, User>(data);

                if (user.Detail == null)
                    user.Detail = new UserDetail();
                _mapper.Map<UserDetailData, UserDetail>(data.Detail, user.Detail);

                user.Id = Guid.NewGuid().ToString();

                var result = await _userService.CreateAsync(user, data.Password).ConfigureAwait(false);
                if (result.Succeeded)
                {
                    await _confirmationService.SendConfirmationMailAsync(user.Id, user.Email).ConfigureAwait(false);
                    return new ActionResponse(MessageBuilder.Successfully, StatusCodes.Status200OK);
                }

                var errorMessage = result.Errors.FirstOrDefault();
                return new ActionResponse(errorMessage, StatusCodes.Status400BadRequest);
            }
            return new ActionResponse(MessageBuilder.UserOrEmailExist, StatusCodes.Status400BadRequest);
        }

        [HttpGet("ConfirmEmail")]
        public async Task<ActionResponse> ConfirmEmail([FromQuery][Required] string userId, [FromQuery][Required] string token)
        {
            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(token))
            {
                var user = await _userService.GetUserByIdAsync(userId);
                if (user.EmailConfirmed)
                    return new ActionResponse(MessageBuilder.EmailAlreadyConfirmed, StatusCodes.Status400BadRequest);

                var result = await _userService.ConfirmEmailAsync(userId, token);
                if (result.Succeeded)
                {
                    await _userService.SetLastEmailRequestDeleted(userId).ConfigureAwait(false);
                    return new ActionResponse(MessageBuilder.EmailConfirmed, StatusCodes.Status200OK);

                }
            }
            return new ActionResponse(MessageBuilder.InvalidToken, StatusCodes.Status400BadRequest);
        }

        [HttpGet("ForgotPassword")]
        public async Task<ActionResponse> ForgotPassword([Required] string email)
        {
            var user = await _userService.GetUserByEmailAsync(email);
            if (user == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status400BadRequest);

            await _confirmationService.SendPasswordResetMailAsync(user.Id, user.Email).ConfigureAwait(false);
            return new ActionResponse();
        }
        [HttpGet("CheckPasswordReset")]
        public async Task<ActionResponse> PasswordReset([FromQuery][Required] string userId)
        {
            var user = await _userService.GetUserByIdAsync(userId, c => c.PasswordResetRequests);
            if (user.PasswordResetRequests.Any())
                return new ActionResponse(MessageBuilder.Success, StatusCodes.Status200OK);

            return new ActionResponse(MessageBuilder.InvalidToken, StatusCodes.Status400BadRequest);
        }
        [HttpPost("PasswordReset")]
        public async Task<ActionResponse> PasswordReset(PasswordReset data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);


            var result = await _userService.ResetPasswordAsync(data.UserId, data.Token, data.Password);
            if (result.Succeeded)
            {
                await _userService.SetLastPasswordRequestDeleted(data.UserId);
                return new ActionResponse(MessageBuilder.PasswordReset, StatusCodes.Status200OK);

            }

            return new ActionResponse(MessageBuilder.InvalidToken, StatusCodes.Status400BadRequest);
        }

    }
}
