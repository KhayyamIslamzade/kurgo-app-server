﻿using Application.Middlewares;
using Application.Services;
using Core.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shared;
using System;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {
        private readonly TokenService _tokenService;
        private readonly UserService _userService;
        private readonly ConfirmationService _confirmationService;
        private readonly EmailService _emailService;
        private readonly IWebHostEnvironment _environment;

        public TestController(TokenService tokenService, UserService userService, ConfirmationService confirmationService, EmailService emailService, IWebHostEnvironment environment)
        {
            _tokenService = tokenService;
            _userService = userService;
            _confirmationService = confirmationService;
            _emailService = emailService;
            _environment = environment;
        }
        [HttpGet("AdminOnly")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Permission(policys: new[] { "admin", "moderator" }, true)]
        public ActionResponse AdminOnly()
        {
            return new ActionResponse(MessageBuilder.Successfully, StatusCodes.Status200OK);
        }
        [HttpGet("GenerateToken")]
        public ActionResponse GenerateToken()
        {
            var userId = "0307aa8a-661c-4e79-a0fa-3bdea3aebcb5";
            var expires = DateTime.Now.AddMinutes(1);
            return new ActionResponse(_tokenService.GenerateToken(userId, expires), MessageBuilder.Successfully, StatusCodes.Status200OK);
        }
        [HttpGet("ValidateToken")]
        public ActionResponse ValidateToken([FromQuery] string token)
        {

            return new ActionResponse(_tokenService.ValidateCurrentToken(token), MessageBuilder.Successfully, StatusCodes.Status200OK);
        }

        [HttpGet("CreateEmailConfirmationToken")]
        public async Task<ActionResponse> CreateEmailConfirmationToken([FromQuery] string userId)
        {
            var token = await _userService.CreateEmailConfirmationTokenAsync(userId);
            return new ActionResponse(token, MessageBuilder.Successfully, StatusCodes.Status200OK);
        }

        [HttpGet("SendMail")]
        public async Task<ActionResponse> SendMailAsync([FromQuery] string email)
        {
            var contentRoot = _environment.ContentRootPath;
            var emailTemplatePath = contentRoot + "\\File\\emailConfirmationTemplate.html";
            //await _confirmationService.SendConfirmationMailAsync(userId, "khayyamislamzade@gmail.com");
            //var confirmationLink = "<a href=\"https://www.google.com\" target=\"_blank\"> www.google.com </a>";
            var content = System.IO.File.ReadAllText(emailTemplatePath);
            var replacedString = content.Replace("@forwardLink", "https://www.google.com");
            await _emailService.SendEmailAsync(new MailModel()
            {
                TargetMail = email,
                Subject = "Please Confirm Your Email",
                Content = replacedString
            });
            return new ActionResponse(MessageBuilder.Successfully, StatusCodes.Status200OK);
        }
    }
}
