﻿using Application.Middlewares;
using AutoMapper;
using AutoWrapper.Extensions;
using Core.Enums;
using Core.Utilities;
using Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Process;
using Process.Repository;
using Shared;
using Shared.Resources.Seller;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CategoryController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Category> _repository;
        private readonly IMapper _mapper;

        public CategoryController(IUnitOfWork unitOfWork, IRepository<Category> repository, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _mapper = mapper;
        }


        [HttpGet("Get/{id}")]
        public async Task<ActionResponse> Get(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);
            if (item != null)
                return new ActionResponse(_mapper.Map<Category, CategoryGetData>(item), StatusCodes.Status200OK);
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

        }
        [HttpGet("GetAll")]
        public async Task<ActionResponse> GetAll([FromQuery] FilterParameters filterParameters, [FromQuery] PagingParameters pagingParameters)
        {
            IQueryable<Category> data;
            if (!string.IsNullOrEmpty(filterParameters.Label))
                data = _repository.FindBy(c => c.Label.ToLower().Contains(filterParameters.Label.ToLower()));
            else
                data = _repository.GetAll();
            var dataCount = data.Count();


            if (!pagingParameters.IsAll)
                data = data.FindPaged(pagingParameters);
            data = data.OrderBy(c => c.Id);

            var result =
                _mapper.Map<List<Category>, List<CategoryGetData>>(await data.ToListAsync().ConfigureAwait(false));
            return new ActionResponse(new
            {
                items = result,
                totalCount = dataCount
            }, StatusCodes.Status200OK);
        }
        [Permission(policys: new string[] { "category_add" })]
        [HttpPost("Add")]
        public async Task<ActionResponse> Add([FromBody] CategoryData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            var item = _mapper.Map<CategoryData, Category>(data);

            await _repository.AddAsync(item).ConfigureAwait(false);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await Get(item.Id).ConfigureAwait(false);
        }
        [Permission(policys: new string[] { "category_edit" })]
        [HttpPost("Edit")]
        public async Task<ActionResponse> Edit([FromBody] CategoryData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            var item = await _repository.GetSingleFirstAsync(c => c.Id == data.Id).ConfigureAwait(false);
            if (item == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

            _mapper.Map(data, item);
            await _unitOfWork.CompleteAsync().ConfigureAwait(false);

            return await Get(item.Id).ConfigureAwait(false);
        }
        [Permission(policys: new string[] { "category_delete" })]
        [HttpPost("Delete/{id}")]
        public async Task<ActionResponse> Delete(int id)
        {
            var item = await _repository.GetSingleFirstAsync(c => c.Id == id).ConfigureAwait(false);

            if (item != null)
            {
                item.Status = (byte)RecordStatus.Deleted;
                await _unitOfWork.CompleteAsync().ConfigureAwait(false);
                return new ActionResponse(MessageBuilder.Deleted(), StatusCodes.Status200OK);
            }
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);
        }
    }
}
