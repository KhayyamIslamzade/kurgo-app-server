﻿using Application.Middlewares;
using AutoMapper;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Process.Repository;
using Shared;
using Shared.Resources.Permission;
using Shared.Resources.PermissionCategory;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Controllers.Identity
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PermissionController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<PermissionCategoryPermission> _repository;
        private readonly IRepository<Permission> _permissionRepository;

        public PermissionController(IRepository<PermissionCategoryPermission> repository, IMapper mapper, IRepository<Permission> permissionRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _permissionRepository = permissionRepository;
        }


        [Permission(policys: new string[] { "role_add", "role_edit" })]
        [HttpGet("GetAll")]
        public async Task<ActionResponse> GetAllAsync()
        {
            var data = _repository.GetAll("Permission", "Category");
            var result =
                _mapper.Map<List<PermissionCategoryPermission>, List<PermissionCategoryRelationGetData>>(await data.ToListAsync()
                    .ConfigureAwait(false));
            return new ActionResponse(result, StatusCodes.Status200OK);
        }

        [Permission(policys: new string[] { "user_add", "user_edit" })]
        [HttpGet("GetAllDirectivePermissions")]
        public async Task<ActionResponse> GetAllDirectivePermissionsAsync()
        {
            var data = _permissionRepository.FindBy(c => c.IsDirective);
            var result =
                _mapper.Map<List<Permission>, List<PermissionGetData>>(await data.ToListAsync()
                    .ConfigureAwait(false));
            return new ActionResponse(result, StatusCodes.Status200OK);
        }

    }
}
