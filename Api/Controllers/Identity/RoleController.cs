﻿using Application.Middlewares;
using Application.Services;
using AutoMapper;
using AutoWrapper.Extensions;
using Core.Constants;
using Core.Utilities;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared;
using Shared.Resources.Role;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Api.Controllers.Identity
{

    [ApiController]
    [Route("api/[controller]")]


    public class RoleController : ControllerBase
    {
        private readonly RoleService _roleService;
        private readonly UserService _userService;
        private readonly IMapper _mapper;

        public RoleController(RoleService roleService, UserService userService, IMapper mapper)
        {
            _roleService = roleService;
            _userService = userService;
            _mapper = mapper;
        }


        [Permission(policys: new string[] { "role_list", "role_edit", "user_add", "user_edit" })]
        [HttpGet("Get/{id}")]
        public async Task<ActionResponse> Get(string id)
        {
            var includeParams = new IncludeStringConstants().RolePermissionIncludeList;
            includeParams.Add("Users.User");
            var role = await _roleService.GetRoleByIdAsync(id, includeParams.ToArray()).ConfigureAwait(false);
            if (role != null)
            {
                var data = _mapper.Map<Role, RoleGetData>(role);
                return new ActionResponse(data);
            }

            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);
        }


        [Permission(policys: new string[] { "role_list", "user_add", "user_edit" })]
        [HttpGet("GetAll")]
        public async Task<ActionResponse> GetAll()
        {
            var includeParams = new IncludeStringConstants().RolePermissionIncludeList;
            includeParams.Add("Users.User");
            var userId = _userService.GetAuthorizedUserId();
            var isAdmin = await _userService.IsAdminAsync(userId);
            Expression<Func<Role, bool>> predicate;
            if (isAdmin)
                predicate = c => true;
            else
                predicate = c =>
                    !string.Equals(c.Name.ToLower(), "admin");
            var role = await _roleService.FindBy(predicate, includeParams.ToArray()).ToListAsync().ConfigureAwait(false);
            var data = _mapper.Map<List<Role>, List<RoleGetData>>(role);
            return new ActionResponse(data);
        }

        [Permission(policys: new string[] { "role_add" })]
        [HttpPost("Add")]
        public async Task<ActionResponse> Add([FromBody] RoleData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());


            if (await _roleService.IsExistAsync(c => c.Name == data.Name).ConfigureAwait(false))
                return new ActionResponse(MessageBuilder.AlreadyExist(data.Name), StatusCodes.Status400BadRequest);

            var role = _mapper.Map<RoleData, Role>(data);
            role.Id = Guid.NewGuid().ToString();
            role.DateCreated = DateTime.Now;
            await _roleService.CreateAsync(role).ConfigureAwait(false);

            return await Get(role.Id).ConfigureAwait(false);
        }

        [Permission(policys: new string[] { "role_edit" })]
        [HttpPost("Edit")]
        public async Task<ActionResponse> Edit([FromBody] RoleData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());

            var role = await _roleService.GetRoleByIdAsync(data.Id, new IncludeStringConstants().RolePermissionIncludeList.ToArray()).ConfigureAwait(false);
            if (role == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

            if (!role.IsEditable)
                return new ActionResponse(MessageBuilder.NotEditable, StatusCodes.Status400BadRequest);

            if (data.Name != role.Name &&
                await _roleService.IsExistAsync(c => c.Name == data.Name).ConfigureAwait(false))
                return new ActionResponse(MessageBuilder.AlreadyExist(data.Name), StatusCodes.Status400BadRequest);


            //update
            _mapper.Map<RoleData, Role>(data, role);
            role.DateModified = DateTime.Now;
            await _roleService.UpdateAsync(role).ConfigureAwait(false);

            return await Get(role.Id).ConfigureAwait(false);
        }

        [Permission(policys: new string[] { "role_delete" })]
        [HttpPost("Delete/{id}")]
        public async Task<ActionResponse> Delete(string id)
        {

            var role = await _roleService.GetRoleByIdAsync(id).ConfigureAwait(false);
            if (role != null)
            {
                if (!role.IsEditable)
                    return new ActionResponse(MessageBuilder.NotEditable, StatusCodes.Status400BadRequest);

                await _roleService.DeleteAsync(role).ConfigureAwait(false);
                return new ActionResponse(MessageBuilder.Deleted());
            }
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

        }

    }
}
