﻿using Application.Middlewares;
using Application.Services;
using AutoMapper;
using AutoWrapper.Extensions;
using Core.Constants;
using Core.Utilities;
using Data.Entities;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Process.Repository;
using Shared;
using Shared.Resources.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers.Identity
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class UserController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly IRepository<UserDetail> _detailRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserController(UserService userService, IRepository<UserDetail> detailRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _userService = userService;
            _detailRepository = detailRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet("Get")]
        public async Task<ActionResponse> Get()
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var item = await _userService.GetUserByNameAsync(User.Identity.Name, includeParams.ToArray()).ConfigureAwait(false);

            if (item != null)
                return new ActionResponse(_mapper.Map<User, UserGetData>(item), StatusCodes.Status200OK);
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

        }
        [Permission(policys: new string[] { "user_edit" })]
        [HttpGet("Get/{id}")]
        public async Task<ActionResponse> Get(string id)
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var item = await _userService.GetUserByIdAsync(id, includeParams.ToArray()).ConfigureAwait(false);
            if (item != null)
                return new ActionResponse(_mapper.Map<User, UserGetData>(item), StatusCodes.Status200OK);
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);

        }
        [Permission(policys: new string[] { "user_list" })]
        [HttpGet("GetAll")]
        public async Task<ActionResponse> GetAll()
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            includeParams.Add("Detail");
            var data = _userService.FindBy(c => c.IsEditable, includeParams.ToArray());
            return new ActionResponse(_mapper.Map<List<User>, List<UserGetData>>(await data.ToListAsync().ConfigureAwait(false)), StatusCodes.Status200OK);

        }

        [HttpGet("GetInfo")]
        public async Task<ActionResponse> GetInfo()
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var userId = _userService.GetAuthorizedUserId();
            var item = await _userService.GetUserByIdAsync(userId, includeParams.ToArray()).ConfigureAwait(false);

            if (item != null)
                return new ActionResponse(_mapper.Map<User, UserGetData>(item));
            return new ActionResponse(MessageBuilder.NotFound);

        }

        [HttpGet("GetUserDetail/{userId}")]
        public async Task<ActionResponse> GetUserDetail(string userId)
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            includeParams.Add("Detail");
            var authorizedUserId = _userService.GetAuthorizedUserId();
            var hasPermission = await _userService.UserIsInPermissionAsync(authorizedUserId, "user_add");
            var isUserIdEqualToAuthorizedUser = _userService.GetAuthorizedUserId() == userId;

            User item = null;
            if (hasPermission || isUserIdEqualToAuthorizedUser)
            {
                item = await _userService.GetUserByIdAsync(userId, includeParams.ToArray()).ConfigureAwait(false);
            }

            if (item != null)
                return new ActionResponse(_mapper.Map<UserDetail, UserDetailGetData>(item.Detail));
            return new ActionResponse(MessageBuilder.NotFound);

        }


        [Permission(policys: new string[] { "user_add" })]
        [HttpPost("Add")]
        public async Task<ActionResponse> Add([FromBody] UserAddData data)
        {

            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());

            if (!await _userService.IsExistAsync(c => c.UserName == data.UserName || c.Email == data.Email)
                .ConfigureAwait(false))
            {
                var user = _mapper.Map<UserAddData, User>(data);

                user.Id = Guid.NewGuid().ToString();
                user.EmailConfirmed = true;

                var result = await _userService.CreateAsync(user, data.Password).ConfigureAwait(false);
                if (result.Succeeded)
                    return await Get(user.Id).ConfigureAwait(false);
                var errorMessage = result.Errors.FirstOrDefault();
                return new ActionResponse(errorMessage, StatusCodes.Status400BadRequest);
            }
            return new ActionResponse(MessageBuilder.UserOrEmailExist, StatusCodes.Status400BadRequest);

        }

        //[HttpPost("UpdateDetail")]
        //public async Task<ActionResponse> AddDetail([FromBody] UserDetailData data)
        //{

        //    if (!ModelState.IsValid)
        //        return new ActionResponse(ModelState.AllErrors());

        //    var hasPermission = await _userService.UserIsInPermissionAsync(data.UserId, "user_add");
        //    var isUserIdEqualToAuthorizedUser = _userService.GetAuthorizedUserId() == data.UserId;

        //    var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
        //    includeParams.Add("Detail");
        //    User user = null;
        //    if (hasPermission || isUserIdEqualToAuthorizedUser)
        //    {
        //        user = await _userService.GetUserByIdAsync(data.UserId, includeParams.ToArray()).ConfigureAwait(false);
        //        var detail = _mapper.Map<UserDetailData, UserDetail>(data);
        //        user.Detail = detail;
        //        user.IsDetailCompleted = true;
        //        await _userService.UpdateAsync(user).ConfigureAwait(false);
        //        return new ActionResponse(StatusCodes.Status200OK);

        //    }
        //    return new ActionResponse(MessageBuilder.NotFound);
        //}

        [HttpPost("UpdateDetail")]
        public async Task<ActionResponse> UpdateDetail([FromBody] UserDetailEditData data)
        {

            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());

            var hasPermission = await _userService.UserIsInPermissionAsync(data.UserId, "user_add");
            var isUserIdEqualToAuthorizedUser = _userService.GetAuthorizedUserId() == data.UserId;

            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();

            var isEdit = false;
            User user = null;
            if (hasPermission || isUserIdEqualToAuthorizedUser)
            {
                user = await _userService.GetUserByIdAsync(data.UserId, includeParams.ToArray()).ConfigureAwait(false);
                if (user != null)
                {
                    var detail = await _detailRepository.GetSingleFirstAsync(c => c.UserId == data.UserId);
                    if (detail == null)
                        detail = new UserDetail();
                    else
                        isEdit = true;
                    _mapper.Map<UserDetailData, UserDetail>(data, detail);
                    if (!isEdit)
                        await _detailRepository.AddAsync(detail).ConfigureAwait(false);
                    await _unitOfWork.CompleteAsync().ConfigureAwait(false);
                    user.IsDetailCompleted = true;
                    await _userService.UpdateAsync(user).ConfigureAwait(false);
                    return new ActionResponse(StatusCodes.Status200OK);
                }

            }
            return new ActionResponse(MessageBuilder.NotFound);
        }
        [Permission(policys: new string[] { "user_edit" })]
        [HttpPost("Edit")]
        public async Task<ActionResponse> Edit([FromBody] UserEditData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());

            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var user = await _userService.GetUserByIdAsync(data.Id, includeParams.ToArray()).ConfigureAwait(false);

            if (user == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);
            if (!user.IsEditable)
                return new ActionResponse(MessageBuilder.NotEditable, StatusCodes.Status400BadRequest);

            //update
            _mapper.Map<UserEditData, User>(data, user);

            await _userService.UpdateAsync(user).ConfigureAwait(false);

            return await Get(user.Id).ConfigureAwait(false);
        }

        [Permission(policys: new string[] { "user_edit" })]
        [HttpPost("ChangePassword")]
        public async Task<ActionResponse> ChangePassword([FromBody] UserChangePasswordData data)
        {
            if (!ModelState.IsValid)
                return new ActionResponse(ModelState.AllErrors());

            var user = await _userService.GetUserByIdAsync(data.Id).ConfigureAwait(false);
            if (user == null)
                return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);
            if (!user.IsEditable)
                return new ActionResponse(MessageBuilder.NotEditable, StatusCodes.Status400BadRequest);


            var changePasswordResult =
                await _userService.ChangePasswordAsync(user, data.OldPassword, data.Password).ConfigureAwait(false);
            if (!changePasswordResult.Succeeded)
            {
                var errorMessage = changePasswordResult.Errors.FirstOrDefault();
                var message = errorMessage == null ? MessageBuilder.Fail : errorMessage.Description;
                return new ActionResponse(message, StatusCodes.Status400BadRequest);
            }
            return new ActionResponse(StatusCodes.Status200OK);

        }

        [Permission(policys: new string[] { "user_delete" })]
        [HttpPost("Delete/{id}")]
        public async Task<ActionResponse> Delete(string id)
        {
            var user = await _userService.GetUserByIdAsync(id).ConfigureAwait(false);
            if (user != null)
            {
                if (!user.IsEditable)
                    return new ActionResponse(MessageBuilder.NotEditable, StatusCodes.Status400BadRequest);

                await _userService.DeleteAsync(user).ConfigureAwait(false);
                return new ActionResponse(MessageBuilder.Deleted(), StatusCodes.Status200OK);

            }
            return new ActionResponse(MessageBuilder.NotFound, StatusCodes.Status404NotFound);
        }

    }
}
