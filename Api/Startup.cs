﻿using Application.Middlewares;
using AutoMapper;
using AutoWrapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Process;
using Shared;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            StaticConfig = configuration;
        }

        public IConfiguration Configuration { get; }
        public static IConfiguration StaticConfig { get; private set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddAutoMapper(typeof(MappingProfile));
            services.Configure<EmailSettings>(Configuration.GetSection("emailSettings"));
            services.Configure<TokenSettings>(Configuration.GetSection("tokenSettings"));
            services.Configure<DomainSettings>(Configuration.GetSection("domainSettings"));
            services.AddHttpClient();

            services.ConfigureServices()
            .ConfigureRepositories()
            .ConfigureDatabase()
            .ConfigureIdentity()
            .ConfigureAuthorization()
            .ConfigureAuthentication();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseApiResponseAndExceptionWrapper(new AutoWrapperOptions
            {
                IsApiOnly = true,
                ShowApiVersion = false,
                ShowStatusCode = true,
                UseCustomSchema = true,
                IgnoreNullValue = false,
            });
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseAuthorization();
            app.UseAuthentication();
            app.UseMiddleware<LoggingMiddleware>();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

        }
    }
}
