﻿using Application.Middlewares;
using Application.Services;
using Data;
using Data.Entities;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Process.Repository;
using Shared;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace Api
{
    public static class ConfigureExtensions
    {
        public static IServiceCollection ConfigureIdentity(this IServiceCollection services)
        {
            services.AddIdentity<User, Role>(
                options =>
                {
                    options.Password.RequiredLength = 8;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireDigit = false;
                    options.Password.RequiredUniqueChars = 0;
                    options.SignIn.RequireConfirmedEmail = true;
                }).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
            return services;
        }
        public static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
        {
            var appSettingsSection = Startup.StaticConfig.GetSection("TokenSettings");
            services.Configure<TokenSettings>(appSettingsSection);
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            var appSettings = appSettingsSection.Get<TokenSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.JwtKey);
            var issuer = appSettings.JwtIssuer;

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.IncludeErrorDetails = true;
                x.RequireHttpsMetadata = false;
                x.SaveToken = false;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = issuer,
                    ValidAudience = issuer,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };
            });

            return services;
        }

        public static IServiceCollection ConfigureAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                var context = services.BuildServiceProvider().GetService<ApplicationDbContext>();
                var categories = context.PermissionCategoryPermissions
                    .Include(c => c.Permission)
                    .Include(c => c.Category);


                foreach (var permissionCategory in categories)
                {
                    //Usage : user_add
                    options.AddPolicy(permissionCategory.Category.Label.ToLower() + "_" + permissionCategory.Permission.Label.ToLower(),
                        policy => policy.Requirements.Add(new PermissionRequirement(
                            new PermissionRequirementModel(permissionCategory.PermissionId, permissionCategory.CategoryId)
                        )));

                }

                var permissions = context.Permissions.Where(c => c.IsDirective).ToList();
                foreach (var permission in permissions)
                {
                    //Usage : admin
                    options.AddPolicy(permission.Label.ToLower(),
                        policy => policy.Requirements.Add(new PermissionRequirement(
                            new PermissionRequirementModel(permission.Label)
                        )));
                }

            });
            return services;
        }


        public static IServiceCollection ConfigureDatabase(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseNpgsql(Startup.StaticConfig.GetConnectionString("DefaultPostgre")));

            //services.AddDbContext<ApplicationDbContext>(
            //options => options.UseSqlServer(Startup.StaticConfig.GetConnectionString("DefaultPostgre")));

            return services;
        }
        public static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<UserRole>, Repository<UserRole>>();
            services.AddScoped<IRepository<UserDetail>, Repository<UserDetail>>();
            services.AddScoped<IRepository<ParcelStatus>, Repository<ParcelStatus>>();
            services.AddScoped<IRepository<ParcelOption>, Repository<ParcelOption>>();
            services.AddScoped<IRepository<Permission>, Repository<Permission>>();
            services.AddScoped<IRepository<UserPermission>, Repository<UserPermission>>();
            services.AddScoped<IRepository<Parcel>, Repository<Parcel>>();
            services.AddScoped<IRepository<Category>, Repository<Category>>();
            services.AddScoped<IRepository<Currency>, Repository<Currency>>();
            services.AddScoped<IRepository<Seller>, Repository<Seller>>();
            services.AddScoped<IRepository<Store>, Repository<Store>>();
            services.AddScoped<IRepository<PermissionCategoryPermission>, Repository<PermissionCategoryPermission>>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            return services;
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<UserService>();
            services.AddScoped<RoleService>();
            services.AddScoped<TokenService>();
            services.AddScoped<UploadService>();
            services.AddScoped<UploadService>();
            services.AddScoped<ExportService>();
            services.AddScoped<ConfirmationService>();
            services.AddScoped<EmailService>();
            services.AddScoped<IAuthorizationHandler, PermissionHandler>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            return services;
        }

    }
}
