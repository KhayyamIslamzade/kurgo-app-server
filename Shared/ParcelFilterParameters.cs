﻿namespace Shared
{
    public class ParcelFilterParameters
    {
        public string TrackingNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string FinNumber { get; set; }
        public string IdentificationNumber { get; set; }
        public string User { get; set; }
        public int? Status { get; set; }
        public string[] DateRange { get; set; }
    }
}
