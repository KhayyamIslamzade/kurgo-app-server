﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.User
{
    public class UserChangePasswordData
    {

        [StringLength(128, ErrorMessage = "Id is not in valid format.")]
        public string Id { get; set; }

        [Required]
        //[RegularExpression(RegexConstants.PasswordRegex,
        //    ErrorMessage = "Old Password is not in valid format.")]
        public string OldPassword { get; set; }
        [Required]
        [RegularExpression(RegexConstants.PasswordRegex,
            ErrorMessage = "Password is not in valid format.")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


    }
}
