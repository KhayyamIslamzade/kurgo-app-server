﻿using Core.Enums;

namespace Shared.Resources.User
{
    public class UserDetailGetData
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }

        public byte Gender { get; set; } = (byte)GenderEnum.Male;
        public string Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public string FinNumber { get; set; }
    }
}
