﻿using Core.Constants;
using Core.Enums;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.User
{
    public class UserDetailData
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxs)]
        public string Name { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxs)]
        public string Surname { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthMd)]
        public string Adress { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxxs)]
        public string PhoneNumber { get; set; }

        public byte Gender { get; set; } = (byte)GenderEnum.Male;
        public string Birthday { get; set; }
        [Required]
        [StringLength(11)]//fixed length
        public string IdentificationNumber { get; set; }
        [Required]
        [StringLength(7)]//fixed length
        public string FinNumber { get; set; }
    }
}
