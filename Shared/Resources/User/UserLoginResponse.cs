﻿namespace Shared.Resources.User
{
    public class UserLoginResponse
    {
        public UserGetData User { get; set; }
        public string Token { get; set; }
    }
}