﻿using Core.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.User
{
    public class UserEditData
    {

        [StringLength(GeneralConstants.LengthXs, ErrorMessage = "Id is not in valid format.")]
        public string Id { get; set; }

        public List<string> Roles { get; set; } = new List<string>();
        public List<string> DirectivePermissions { get; set; } = new List<string>();

    }
}
