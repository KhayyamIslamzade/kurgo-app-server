﻿using Shared.Resources.Permission;
using Shared.Resources.Role;
using System.Collections.Generic;

namespace Shared.Resources.User
{
    public class UserGetData
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool IsEditable { get; set; }
        public bool IsDetailCompleted { get; set; }
        public int Status { get; set; }
        public string DateCreated { get; set; }
        public List<RoleGetData> Roles { get; set; } = new List<RoleGetData>();
        public List<PermissionGetData> DirectivePermissions { get; set; } = new List<PermissionGetData>();

    }
}
