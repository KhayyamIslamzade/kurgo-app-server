﻿using Core.Constants;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.User
{
    public class PasswordReset
    {


        [StringLength(128, ErrorMessage = "Id is not in valid format.")]
        public string UserId { get; set; }
        [Required]
        public string Token { get; set; }

        [RegularExpression(RegexConstants.PasswordRegex,
            ErrorMessage = "Password is not in valid format.")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


    }
}
