﻿namespace Shared.Resources.User
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public UserDetailGetData Detail { get; set; }
        public string DateCreated { get; set; }

    }
}
