﻿namespace Shared.Resources.Seller
{
    public class CurrencyData
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
    }
}
