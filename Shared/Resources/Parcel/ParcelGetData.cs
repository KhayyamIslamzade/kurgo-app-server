﻿using Shared.Resources.Seller;
using Shared.Resources.User;
using System.Collections.Generic;

namespace Shared.Resources.Parcel
{
    public class ParcelGetData
    {
        public int Id { get; set; }
        public string TrackingNumber { get; set; }
        public SellerGetData Seller { get; set; }
        public CategoryGetData Category { get; set; }
        public CurrencyGetData Currency { get; set; }
        public ParcelOptionGetData ParcelOption { get; set; }
        public UserInfo User { get; set; }
        public double ProductPrice { get; set; }
        public int Quantity { get; set; }
        public ParcelStatusInfo ParcelStatus { get; set; }
        public List<ParcelStatusInfo> ParcelStatuses { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
    }
}
