﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Parcel
{
    public class ParcelData
    {
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string TrackingNumber { get; set; }
        public int Quantity { get; set; }
        public double ProductPrice { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public int SellerId { get; set; }
        public int CurrencyId { get; set; }
        public ParcelStatusInfo ParcelStatus { get; set; }
    }
}
