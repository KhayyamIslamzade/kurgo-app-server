﻿namespace Shared.Resources.Parcel
{
    public class ParcelOptionData
    {
        public int ParcelId { get; set; }
        public double TransportPrice { get; set; }
        public int CurrencyId { get; set; }
        public double Weight { get; set; }
    }
}
