﻿namespace Shared.Resources.Parcel
{
    public class ParcelStatusInfo
    {
        public int Id { get; set; }
        public int ParcelId { get; set; }
        public byte StatusOfParcel { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
    }
}
