﻿using Shared.Resources.Seller;

namespace Shared.Resources.Parcel
{
    public class ParcelOptionGetData
    {
        public double TransportPrice { get; set; }
        public CurrencyGetData Currency { get; set; }
        public double Weight { get; set; }
    }
}
