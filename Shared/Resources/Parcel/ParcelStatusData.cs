﻿using Core.Constants;
using Core.Enums;
using Core.Utilities.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Parcel
{
    public class ParcelStatusData
    {
        public int ParcelId { get; set; }
        [IntArrayLimit(
            (int)ParcelStatusEnum.Delivered,
            (int)ParcelStatusEnum.Canceled,
            (int)ParcelStatusEnum.InOffice,
            (int)ParcelStatusEnum.OnProcess,
            (int)ParcelStatusEnum.Transporting,
            (int)ParcelStatusEnum.WaitingApprove
            , ErrorMessage = "Data must be 1,2,3,4,5,6"
            )]
        public byte StatusOfParcel { get; set; } = (byte)ParcelStatusEnum.OnProcess;
        [StringLength(GeneralConstants.LengthLg)]
        public string Description { get; set; }
    }
}
