﻿namespace Shared.Resources.Seller
{
    public class SellerGetData
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string DateCreated { get; set; }
    }
}
