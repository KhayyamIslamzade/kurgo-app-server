﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Seller
{
    public class SellerData
    {
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Label { get; set; }
        [StringLength(GeneralConstants.LengthLg)]
        public string Description { get; set; }
    }
}
