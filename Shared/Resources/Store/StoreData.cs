﻿using Core.Constants;
using System.ComponentModel.DataAnnotations;

namespace Shared.Resources.Store
{
    public class StoreData
    {
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Country { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string City { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthMd)]
        public string Adress { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxxs)]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxxs)]
        public string ZipCode { get; set; }
    }
}
