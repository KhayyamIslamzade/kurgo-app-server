﻿namespace Shared.Resources.Store
{
    public class StoreGetData
    {
        public int Id { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Adress { get; set; }
        public string PhoneNumber { get; set; }
        public string ZipCode { get; set; }

        public string DateCreated { get; set; }
    }
}
