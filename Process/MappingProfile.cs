﻿using AutoMapper;
using Core.Extensions;
using Data.Entities;
using Data.Entities.Identity;
using Shared.Resources.Parcel;
using Shared.Resources.Permission;
using Shared.Resources.PermissionCategory;
using Shared.Resources.Role;
using Shared.Resources.Seller;
using Shared.Resources.Store;
using Shared.Resources.User;
using System;
using System.Linq;

namespace Process
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region User
            CreateMap<User, UserGetData>()
                .ForMember(er => er.Roles,
                    opt => opt.MapFrom(e => e.Roles.Select(c => c.Role)))
                .ForMember(er => er.DirectivePermissions,
                    opt => opt.MapFrom(e => e.DirectivePermissions.Select(c => c.Permission)))
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)))
                ;
            CreateMap<User, UserInfo>()
                    .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)))
                ;
            CreateMap<UserRegisterData, User>();


            CreateMap<UserAddData, User>()
                .ForMember(c => c.Roles, opt => opt.Ignore())
                .ForMember(c => c.DirectivePermissions, opt => opt.Ignore())
                .AfterMap((userAdd, user) =>
                {

                    var addedRoles = userAdd.Roles.Where(id => user.Roles.All(f => f.RoleId != id)).Select(c => new UserRole() { RoleId = c, DateCreated = DateTime.Now });
                    foreach (var item in addedRoles)
                        user.Roles.Add(item);

                    var addedPermissions = userAdd.DirectivePermissions.Where(id => user.DirectivePermissions.All(f => f.PermissionId != id)).Select(c => new UserPermission() { PermissionId = c, DateCreated = DateTime.Now });
                    foreach (var item in addedPermissions)
                        user.DirectivePermissions.Add(item);
                });
            CreateMap<UserEditData, User>()
                .ForMember(c => c.Roles, opt => opt.Ignore())
                .ForMember(c => c.DirectivePermissions, opt => opt.Ignore())
                .AfterMap((userAdd, user) =>
                {
                    var removedRoles = user.Roles.Where(f => !userAdd.Roles.Contains(f.RoleId)).ToList();
                    foreach (var item in removedRoles)
                    {
                        user.Roles.Remove(item);
                    }

                    if (userAdd.Roles != null && userAdd.Roles.Any())
                    {
                        var addedRoles = userAdd.Roles.Where(id => user.Roles.All(f => f.RoleId != id)).Select(c => new UserRole() { RoleId = c, DateCreated = DateTime.Now }).ToList();
                        foreach (var item in addedRoles)
                        {
                            user.Roles.Add(item);
                        }
                    }


                    var removedPermissions = user.DirectivePermissions.Where(f => !userAdd.DirectivePermissions.Contains(f.PermissionId)).ToList();
                    foreach (var item in removedPermissions)
                    {
                        user.DirectivePermissions.Remove(item);
                    }

                    if (userAdd.DirectivePermissions != null && userAdd.DirectivePermissions.Any())
                    {
                        var addedPermissions = userAdd.DirectivePermissions.Where(id => user.DirectivePermissions.All(f => f.PermissionId != id)).Select(c => new UserPermission() { PermissionId = c, DateCreated = DateTime.Now }).ToList();
                        foreach (var item in addedPermissions)
                        {
                            user.DirectivePermissions.Add(item);
                        }
                    }
                });
            #endregion

            #region User Detail

            CreateMap<UserDetail, UserDetailGetData>().ForMember(c => c.Birthday, opt => opt.MapFrom(m => m.Birthday.ToCustomFormatString(false)));
            CreateMap<UserDetailData, UserDetail>().ForMember(c => c.Birthday, opt => opt.MapFrom(m => m.Birthday.ToCustomDateTime()));

            #endregion
            #region Role
            CreateMap<Role, RoleGetData>()
                .ForMember(c => c.Permissions, opt => opt.MapFrom(m => m.PermissionCategory.Select(c => c.PermissionCategoryPermission)))
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<RoleData, Role>()
                .ForMember(c => c.PermissionCategory, opt => opt.Ignore())
                .AfterMap((roleAdd, role) =>
                {
                    var removedPermissions = role.PermissionCategory.Where(f => !roleAdd.PermissionCategories.Contains(f.PermissionCategoryPermissionId)).ToList();
                    foreach (var item in removedPermissions)
                    {
                        role.PermissionCategory.Remove(item);
                    }

                    if (roleAdd.PermissionCategories != null && roleAdd.PermissionCategories.Any())
                    {
                        var addedPermissions = roleAdd.PermissionCategories.Where(id => role.PermissionCategory.All(f => f.PermissionCategoryPermissionId != id)).Select(c => new RolePermissionCategory() { PermissionCategoryPermissionId = c, DateCreated = DateTime.Now }).ToList();
                        foreach (var item in addedPermissions)
                        {
                            role.PermissionCategory.Add(item);
                        }
                    }


                });

            #endregion

            #region Permission

            CreateMap<Permission, PermissionGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            #endregion   
            #region Permission Category

            CreateMap<PermissionCategory, PermissionCategoryGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<Permission, PermissionGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));


            CreateMap<PermissionCategoryPermission, PermissionCategoryRelationGetData>()
                .ForMember(c => c.RelationId, opt => opt.MapFrom(m => m.Id))
                .ForMember(c => c.Category, opt => opt.MapFrom(m => m.Category))
                .ForMember(c => c.Permission, opt => opt.MapFrom(m => m.Permission))
                ;
            #endregion
            #region Parcel
            CreateMap<Parcel, ParcelGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(true)));
            CreateMap<ParcelData, Parcel>();
            CreateMap<ParcelOptionData, Parcel>();
            #endregion
            #region ParcelStatus

            CreateMap<ParcelStatus, ParcelStatusInfo>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<ParcelStatusData, ParcelStatus>();
            #endregion
            #region ParcelStatus

            CreateMap<ParcelOption, ParcelOptionGetData>();

            CreateMap<ParcelOptionData, ParcelOption>();
            #endregion
            #region Category
            CreateMap<Category, CategoryGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<CategoryData, Category>();
            #endregion
            #region Seller
            CreateMap<Seller, SellerGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<SellerData, Seller>();
            #endregion  
            #region Seller
            CreateMap<Currency, CurrencyGetData>()
                .ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<CurrencyData, Currency>();
            #endregion
            #region Store
            CreateMap<Store, StoreGetData>().ForMember(c => c.DateCreated, opt => opt.MapFrom(m => m.DateCreated.ToCustomFormatString(false)));
            CreateMap<StoreData, Store>();
            #endregion

        }
    }
}
