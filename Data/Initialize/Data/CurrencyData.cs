﻿using Data.Entities;
using System.Collections.Generic;

namespace Data.Initialize.Data
{
    public static partial class InitializeData
    {
        public static List<Currency> BuildCurrencyList()
        {
            var list = new List<Currency>()
            {
                new Currency()
                {
                    Label = "TRY"
                },
                new Currency()
                {
                    Label = "AZN"
                },
                new Currency()
                {
                    Label = "USD"
                }
            };
            return list;

        }
    }
}
