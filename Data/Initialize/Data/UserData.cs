﻿using Data.Entities.Identity;
using System;
using System.Collections.Generic;

namespace Data.Initialize.Data
{
    public static partial class InitializeData
    {

        public static List<User> BuildUserList()
        {
            var list = new List<User>()
            {
                new User { Id = Guid.NewGuid().ToString(), UserName = "superadmin", Email = "superadmin@gmail.com", DateCreated = DateTime.Now  ,EmailConfirmed = true, IsEditable = false},
            };
            return list;

        }

    }
}
