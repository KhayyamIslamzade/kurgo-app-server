﻿using Data.Entities;
using System.Collections.Generic;

namespace Data.Initialize.Data
{
    public static partial class InitializeData
    {
        public static List<Seller> BuildSellerList()
        {
            var list = new List<Seller>()
            {
                new Seller {Label = "10amfactory"}, new Seller {Label = "1v1y.com"}, new Seller {Label = "37numara"},
                new Seller {Label = "5in1canpolat"}, new Seller {Label = "Abiyeelbisecim"},
                new Seller {Label = "Abiyefon"}, new Seller {Label = "Abiyemerkezi"}, new Seller {Label = "Accort"},
                new Seller {Label = "Addax"}, new Seller {Label = "Adellakozmetik"}, new Seller {Label = "Adidas"},
                new Seller {Label = "ADL"}, new Seller {Label = "Afrodizyaklar"}, new Seller {Label = "Aisetesettur"},
                new Seller {Label = "Akakce"}, new Seller {Label = "Aksesuarix"},
                new Seller {Label = "Altinyildizclassics"}, new Seller {Label = "Aniyuzuk"},
                new Seller {Label = "Askatesi"}, new Seller {Label = "Askinisantasi"},
                new Seller {Label = "Atolyelavanta"}, new Seller {Label = "Atolyeno6"}, new Seller {Label = "Avansas"},
                new Seller {Label = "Avva"}, new Seller {Label = "Ayakkabiefendisi"},
                new Seller {Label = "Ayakkabiprensi"}, new Seller {Label = "Ayakland"},
                new Seller {Label = "Aysenatasarim"}, new Seller {Label = "Bacaksekillendirici"},
                new Seller {Label = "Badebutik"}, new Seller {Label = "Banana republic"},
                new Seller {Label = "Bayildim"}, new Seller {Label = "Bebelon"}, new Seller {Label = "Bebeshop"},
                new Seller {Label = "Beeo"}, new Seller {Label = "Behicesaglam"}, new Seller {Label = "Belinayicgiyim"},
                new Seller {Label = "Benetton"}, new Seller {Label = "Benimolsun"}, new Seller {Label = "Bershka"},
                new Seller {Label = "Betashoes"}, new Seller {Label = "betashoes.com"}, new Seller {Label = "Beymen"},
                new Seller {Label = "Beymenclub"}, new Seller {Label = "Beyyoglu"}, new Seller {Label = "Bgstore"},
                new Seller {Label = "Bigdart"}, new Seller {Label = "Bilcee"}, new Seller {Label = "Bilge Kırıktaş"},
                new Seller {Label = "Bkmkitap"}, new Seller {Label = "Blacknoble"},
                new Seller {Label = "Bolesasuadiye"}, new Seller {Label = "Bondnon"}, new Seller {Label = "Bonprix"},
                new Seller {Label = "boutiquen.com.tr"}, new Seller {Label = "boyner"},
                new Seller {Label = "Brandroom"}, new Seller {Label = "brcfashion"}, new Seller {Label = "Browwauw"},
                new Seller {Label = "BSL"}, new Seller {Label = "Btmmoda"}, new Seller {Label = "Bulsakalsak"},
                new Seller {Label = "Bunubanayolla"}, new Seller {Label = "Burcutesettur"},
                new Seller {Label = "Butikbira"}, new Seller {Label = "Butikemine"}, new Seller {Label = "Butikeros"},
                new Seller {Label = "Butikmerveaksoy"}, new Seller {Label = "Butikvanilya"},
                new Seller {Label = "Buyukanneminsandigi"}, new Seller {Label = "Buyukbedenelbise"},
                new Seller {Label = "Buyukbedeniz"}, new Seller {Label = "Byselincigerci"},
                new Seller {Label = "Cabani"}, new Seller {Label = "cacharel"}, new Seller {Label = "Cantaponcik"},
                new Seller {Label = "Caponeoutfitters"}, new Seller {Label = "Cappmoda"}, new Seller {Label = "Carmen"},
                new Seller {Label = "Carpe"}, new Seller {Label = "Cartersoshkosh"}, new Seller {Label = "Cevherat"},
                new Seller {Label = "Ceyizdiyari"}, new Seller {Label = "Ceyizedairhersey"},
                new Seller {Label = "Chamakhbutik"}, new Seller {Label = "Chamakhbutik"}, new Seller {Label = "Chetic"},
                new Seller {Label = "Chima"}, new Seller {Label = "Ciceksepeti"}, new Seller {Label = "Cilekbutik"},
                new Seller {Label = "Cimcimebebe"}, new Seller {Label = "Civilim"}, new Seller {Label = "Clinique"},
                new Seller {Label = "COLİN`S"}, new Seller {Label = "Collezione"}, new Seller {Label = "Comertshoes"},
                new Seller {Label = "Corapfestivali"}, new Seller {Label = "Coraptoptancisi"},
                new Seller {Label = "Cottonmood"}, new Seller {Label = "dagi.com"}, new Seller {Label = "Damattween"},
                new Seller {Label = "Davidjones"}, new Seller {Label = "Decathlon"}, new Seller {Label = "Deery"},
                new Seller {Label = "DeFacto"}, new Seller {Label = "Delisiyim"}, new Seller {Label = "denizbutik"},
                new Seller {Label = "Deppoavantaj"}, new Seller {Label = "Deriden"}, new Seller {Label = "Derimod"},
                new Seller {Label = "Deripabuc"}, new Seller {Label = "Dermoeczanem"},
                new Seller {Label = "Dermokozmetika"}, new Seller {Label = "Desa"},
                new Seller {Label = "Devrekbastonlari"}, new Seller {Label = "dhgate"},
                new Seller {Label = "Dilarabutik"}, new Seller {Label = "Dilvin"}, new Seller {Label = "Dioxtea"},
                new Seller {Label = "Divarese"}, new Seller {Label = "Dolce-gusto"}, new Seller {Label = "dr.com.tr"},
                new Seller {Label = "Durununbutigi"}, new Seller {Label = "Dusunbilkitap"},
                new Seller {Label = "D`S damat"}, new Seller {Label = "E-bebek"}, new Seller {Label = "Eatalyshoes"},
                new Seller {Label = "Ebrumaternity"}, new Seller {Label = "Ecanta"},
                new Seller {Label = "Eceninbutigi"}, new Seller {Label = "Eczane"}, new Seller {Label = "Edataspinar"},
                new Seller {Label = "Efbutik"}, new Seller {Label = "Egzotikangels"}, new Seller {Label = "Ekolonline"},
                new Seller {Label = "Elelebaby"}, new Seller {Label = "Elfidatesettur"},
                new Seller {Label = "Elizamoda"}, new Seller {Label = "Elle Shoes"}, new Seller {Label = "Eminosbutik"},
                new Seller {Label = "Emsan"}, new Seller {Label = "Emsan"}, new Seller {Label = "Enbipharma"},
                new Seller {Label = "Englishhome"}, new Seller {Label = "Ervaninbutigi"},
                new Seller {Label = "Esrabastug"}, new Seller {Label = "eveshop"}, new Seller {Label = "Evyapshop"},
                new Seller {Label = "Existsilver"}, new Seller {Label = "Eyupsabrituncer"}, new Seller {Label = "Eyyo"},
                new Seller {Label = "Faiksonmez"}, new Seller {Label = "Faststep"}, new Seller {Label = "Fenerium"},
                new Seller {Label = "Fidankitap"}, new Seller {Label = "Fiseason"}, new Seller {Label = "Fitbas.com"},
                new Seller {Label = "FLO"}, new Seller {Label = "Foxayakkabi"}, new Seller {Label = "Fullamoda"},
                new Seller {Label = "GAP"}, new Seller {Label = "Gesmoda"}, new Seller {Label = "Gigis"},
                new Seller {Label = "gittigidiyor.com"}, new Seller {Label = "Gizyomoda"},
                new Seller {Label = "Goopac.store"}, new Seller {Label = "Gozlukcu"}, new Seller {Label = "Gratis"},
                new Seller {Label = "Greyder"}, new Seller {Label = "GSStore"},
                new Seller {Label = "Gulsahintakitezhahi"}, new Seller {Label = "Guneskitabevi"},
                new Seller {Label = "Gustoeshop"}, new Seller {Label = "H&M"}, new Seller {Label = "H&M Turkey"},
                new Seller {Label = "Hairtox"}, new Seller {Label = "Happy"}, new Seller {Label = "Hapshoe"},
                new Seller {Label = "Hasad"}, new Seller {Label = "Hataydanal"}, new Seller {Label = "Hatemoglu"},
                new Seller {Label = "Havos"}, new Seller {Label = "Haydigiy"}, new Seller {Label = "Hazelanna"},
                new Seller {Label = "hbbagstore"}, new Seller {Label = "Hediyemen"}, new Seller {Label = "Hepsiburada"},
                new Seller {Label = "Hobidunya"}, new Seller {Label = "Hollylolly"}, new Seller {Label = "Hotiç"},
                new Seller {Label = "Humabebe"}, new Seller {Label = "Hypetr"}, new Seller {Label = "İfondi"},
                new Seller {Label = "Ikea"}, new Seller {Label = "Ikoshoe"}, new Seller {Label = "İloveshoes"},
                new Seller {Label = "Ilvi"}, new Seller {Label = "İnna"}, new Seller {Label = "Instreet"},
                new Seller {Label = "Ipeksekerciler"}, new Seller {Label = "Ipekyol"},
                new Seller {Label = "Istasyongiyim"}, new Seller {Label = "Jammybaby"}, new Seller {Label = "Jimmykey"},
                new Seller {Label = "Journey"}, new Seller {Label = "Justinbeaty"}, new Seller {Label = "Kadinmodasi"},
                new Seller {Label = "Karaca-home"}, new Seller {Label = "Kartalyuvasi"}, new Seller {Label = "Kayra"},
                new Seller {Label = "kemal tanca"}, new Seller {Label = "Kigılı"}, new Seller {Label = "Kikiriki"},
                new Seller {Label = "Kip"}, new Seller {Label = "Kirmizionline"}, new Seller {Label = "Kitapsec"},
                new Seller {Label = "Kitapyurdu"}, new Seller {Label = "Kokopatik"}, new Seller {Label = "Kom"},
                new Seller {Label = "Kombincim"}, new Seller {Label = "Konudukkan"}, new Seller {Label = "Kostebek"},
                new Seller {Label = "KOTON"}, new Seller {Label = "koton.com"}, new Seller {Label = "Kozmela"},
                new Seller {Label = "Kozmobox"}, new Seller {Label = "Kshop"}, new Seller {Label = "Kubrabutik"},
                new Seller {Label = "Lafaba"}, new Seller {Label = "Laluvia"}, new Seller {Label = "Laminta"},
                new Seller {Label = "Laranor"}, new Seller {Label = "Lavinyaa"}, new Seller {Label = "LC  Waikiki"},
                new Seller {Label = "Lego.Storeturkey"}, new Seller {Label = "Lensbak"}, new Seller {Label = "Lescon"},
                new Seller {Label = "Lidyana"}, new Seller {Label = "Lilasxxl"}, new Seller {Label = "Limoya"},
                new Seller {Label = "Loft.com"}, new Seller {Label = "Lookadikoy"}, new Seller {Label = "Lovemybody"},
                new Seller {Label = "Loya"}, new Seller {Label = "LTBjeans"}, new Seller {Label = "Lufian"},
                new Seller {Label = "Lutvelizade"}, new Seller {Label = "Machka"},
                new Seller {Label = "Maciterkozmetik"}, new Seller {Label = "Madame Coco"},
                new Seller {Label = "MANGO"}, new Seller {Label = "Marjin"}, new Seller {Label = "Markafoni"},
                new Seller {Label = "Marksandspencer"}, new Seller {Label = "Maskbutik"},
                new Seller {Label = "massimodutti"}, new Seller {Label = "Matbutikshop"},
                new Seller {Label = "Matmazel"}, new Seller {Label = "Mavi.com"}, new Seller {Label = "Maymoda"},
                new Seller {Label = "MAYNATURA"}, new Seller {Label = "Mayox"}, new Seller {Label = "Mazibutik"},
                new Seller {Label = "Merinaturel"}, new Seller {Label = "Mervellion"},
                new Seller {Label = "Metkobutik"}, new Seller {Label = "Michael Kors.global"},
                new Seller {Label = "Milanoor"}, new Seller {Label = "Minetanbutik"}, new Seller {Label = "Minicity"},
                new Seller {Label = "Minikterzi"}, new Seller {Label = "Minipicco"}, new Seller {Label = "Miniso"},
                new Seller {Label = "Mispacoz"}, new Seller {Label = "Missha"}, new Seller {Label = "Moda1001"},
                new Seller {Label = "ModaALAbutik"}, new Seller {Label = "Modaayakkabi"},
                new Seller {Label = "Modacelikler"}, new Seller {Label = "Modailgi"},
                new Seller {Label = "Modakapimda"}, new Seller {Label = "Modakasmir"},
                new Seller {Label = "Modamizbir"}, new Seller {Label = "modanisa"},
                new Seller {Label = "Modasahtesettur"}, new Seller {Label = "Modaselvim"},
                new Seller {Label = "Modasena"}, new Seller {Label = "Modatrend"}, new Seller {Label = "Modavitrini"},
                new Seller {Label = "Monsoonlondon"}, new Seller {Label = "Mooibutik"}, new Seller {Label = "Morhipo"},
                new Seller {Label = "Mossta"}, new Seller {Label = "Mudo"}, new Seller {Label = "Mupashoes"},
                new Seller {Label = "mydukkan"}, new Seller {Label = "Mylovebutik"},
                new Seller {Label = "Mypoppishoes"}, new Seller {Label = "Mysilvers"}, new Seller {Label = "Myvitrinn"},
                new Seller {Label = "n11.com"}, new Seller {Label = "Nadirkitap"}, new Seller {Label = "Naiaistanbul"},
                new Seller {Label = "Naramaxx"}, new Seller {Label = "Narferita"}, new Seller {Label = "Neroxtea"},
                new Seller {Label = "Neslihancanpolat"}, new Seller {Label = "NetWork"},
                new Seller {Label = "Newbalance"}, new Seller {Label = "Newobsessions"},
                new Seller {Label = "Nextdirect"}, new Seller {Label = "Nike"}, new Seller {Label = "Ninewest"},
                new Seller {Label = "Nisantasishoes"}, new Seller {Label = "Nobelkitabevi"},
                new Seller {Label = "nocturne"}, new Seller {Label = "Nofilterbutik"}, new Seller {Label = "Nonabutik"},
                new Seller {Label = "Oblavion"}, new Seller {Label = "Occaasion"}, new Seller {Label = "Onespraytatoo"},
                new Seller {Label = "Onukgiyim"}, new Seller {Label = "Onzemode"}, new Seller {Label = "Oxxo"},
                new Seller {Label = "Oynasana"}, new Seller {Label = "Oysho"}, new Seller {Label = "Ozdilekteyim"},
                new Seller {Label = "Ozgeozgenc"}, new Seller {Label = "Paandabutik"}, new Seller {Label = "Panco"},
                new Seller {Label = "Pantopark"}, new Seller {Label = "patırtı"},
                new Seller {Label = "Payidarhomeconcept"}, new Seller {Label = "Pelininayakkabilari"},
                new Seller {Label = "Pelinsenoglu"}, new Seller {Label = "Penti"}, new Seller {Label = "Penyemood"},
                new Seller {Label = "Peraboutiqque"}, new Seller {Label = "Perspective"},
                new Seller {Label = "Pierre Cardin"}, new Seller {Label = "Pinarsboutique"},
                new Seller {Label = "Pixylove"}, new Seller {Label = "Podyumplus"}, new Seller {Label = "Popoptik"},
                new Seller {Label = "Poshet"}, new Seller {Label = "Pudrapayet"}, new Seller {Label = "Pullandbear"},
                new Seller {Label = "Puma"}, new Seller {Label = "Pumpsup"}, new Seller {Label = "Purplebybanu"},
                new Seller {Label = "Purpuravioleta"}, new Seller {Label = "Quzu"}, new Seller {Label = "Ramsey"},
                new Seller {Label = "Relaxmode"}, new Seller {Label = "Replikayakkabi"},
                new Seller {Label = "Retrobird"}, new Seller {Label = "Rhytmine"}, new Seller {Label = "Roman"},
                new Seller {Label = "Rossmann"}, new Seller {Label = "Saatevi"}, new Seller {Label = "Saatkordoncusu"},
                new Seller {Label = "Sabunique"}, new Seller {Label = "Sahibinden"}, new Seller {Label = "Saklibutik"},
                new Seller {Label = "Saklibutik"}, new Seller {Label = "Saklibutik"}, new Seller {Label = "Saklibutik"},
                new Seller {Label = "Sateen"}, new Seller {Label = "Saygigiyim"}, new Seller {Label = "Secilstore"},
                new Seller {Label = "Sedatutulmaz"}, new Seller {Label = "Sedayalcinatelier"},
                new Seller {Label = "Sefamerve"}, new Seller {Label = "Selectmoda"}, new Seller {Label = "Selinshoes"},
                new Seller {Label = "Seninfarkin"}, new Seller {Label = "Seninolsun"},
                new Seller {Label = "Sentetiksezar"}, new Seller {Label = "Sephora"}, new Seller {Label = "Serbella"},
                new Seller {Label = "Setre"}, new Seller {Label = "Shein.tr."}, new Seller {Label = "Sheloveayakkabi"},
                new Seller {Label = "Shemdi"}, new Seller {Label = "shoe tek"}, new Seller {Label = "Shoebutik"},
                new Seller {Label = "Shoetekfiyat"}, new Seller {Label = "shop.adidas"},
                new Seller {Label = "Shop.sarar"}, new Seller {Label = "Shopier"}, new Seller {Label = "Sibeldurmaz"},
                new Seller {Label = "Silesilver"}, new Seller {Label = "Simpleandchicco.dukkan"},
                new Seller {Label = "Sindimoda"}, new Seller {Label = "Sisistanbul"},
                new Seller {Label = "Siyahincionline"}, new Seller {Label = "Skechers"},
                new Seller {Label = "Socksstations"}, new Seller {Label = "Soflycosmetics"},
                new Seller {Label = "Sogutlusilver"}, new Seller {Label = "Sokakbutik"},
                new Seller {Label = "Solomombaby"}, new Seller {Label = "Soobe"}, new Seller {Label = "sportive.com"},
                new Seller {Label = "Spotbalk"}, new Seller {Label = "Stilask"}, new Seller {Label = "Stradivarius"},
                new Seller {Label = "Straswans"}, new Seller {Label = "Supermino"}, new Seller {Label = "Suvari"},
                new Seller {Label = "tarzkiz.com"}, new Seller {Label = "Tazekuru"}, new Seller {Label = "Tekstilev"},
                new Seller {Label = "Tergan"}, new Seller {Label = "Terrarossabutik"},
                new Seller {Label = "Tesetturdunyasi"}, new Seller {Label = "Tesetturpazari"},
                new Seller {Label = "Thebodyshop"}, new Seller {Label = "Tiamoda"}, new Seller {Label = "Tisho"},
                new Seller {Label = "Tisort.ist"}, new Seller {Label = "tofisa"}, new Seller {Label = "Tommylife"},
                new Seller {Label = "toyzzshop"}, new Seller {Label = "Trendbende"}, new Seller {Label = "Trendstore"},
                new Seller {Label = "Trendyes"}, new Seller {Label = "Trendyol"}, new Seller {Label = "Trikomisirli"},
                new Seller {Label = "Tubabutik"}, new Seller {Label = "Tuesbutik"}, new Seller {Label = "Tugbavenn"},
                new Seller {Label = "Tutyakala"}, new Seller {Label = "Tuvid"}, new Seller {Label = "Twist"},
                new Seller {Label = "U.S. POLO ASSN (TR)"}, new Seller {Label = "Ulkucumarket"},
                new Seller {Label = "Uludagtriko"}, new Seller {Label = "Upwatch"}, new Seller {Label = "Urbandecay"},
                new Seller {Label = "Urbanmuse"}, new Seller {Label = "Vekem"}, new Seller {Label = "Viadellerose"},
                new Seller {Label = "Vicco"}, new Seller {Label = "Vitmadogalgida"},
                new Seller {Label = "Vizonayakkabi"}, new Seller {Label = "Wahshe"}, new Seller {Label = "Watsons"},
                new Seller {Label = "Wcollection"}, new Seller {Label = "Wlabkozmetik"},
                new Seller {Label = "Yakestore"}, new Seller {Label = "Ydsshop"}, new Seller {Label = "Yer6store.com"},
                new Seller {Label = "Yvesrocher"}, new Seller {Label = "ZARA"}, new Seller {Label = "Zeqmoda"},
                new Seller {Label = "Zoombutik"}, new Seller {Label = ""}, new Seller {Label = "10amfactory"},
                new Seller {Label = "1v1y.com"}, new Seller {Label = "37numara"}, new Seller {Label = "5in1canpolat"},
                new Seller {Label = "Abiyeelbisecim"}, new Seller {Label = "Abiyefon"},
                new Seller {Label = "Abiyemerkezi"}, new Seller {Label = "Accort"}, new Seller {Label = "Addax"},
                new Seller {Label = "Adellakozmetik"}, new Seller {Label = "Adidas"}, new Seller {Label = "ADL"},
                new Seller {Label = "Afrodizyaklar"}, new Seller {Label = "Aisetesettur"},
                new Seller {Label = "Akakce"}, new Seller {Label = "Aksesuarix"},
                new Seller {Label = "Altinyildizclassics"}, new Seller {Label = "Aniyuzuk"},
                new Seller {Label = "Askatesi"}, new Seller {Label = "Askinisantasi"},
                new Seller {Label = "Atolyelavanta"}, new Seller {Label = "Atolyeno6"}, new Seller {Label = "Avansas"},
                new Seller {Label = "Avva"}, new Seller {Label = "Ayakkabiefendisi"},
                new Seller {Label = "Ayakkabiprensi"}, new Seller {Label = "Ayakland"},
                new Seller {Label = "Aysenatasarim"}, new Seller {Label = "Bacaksekillendirici"},
                new Seller {Label = "Badebutik"}, new Seller {Label = "Banana republic"},
                new Seller {Label = "Bayildim"}, new Seller {Label = "Bebelon"}, new Seller {Label = "Bebeshop"},
                new Seller {Label = "Beeo"}, new Seller {Label = "Behicesaglam"}, new Seller {Label = "Belinayicgiyim"},
                new Seller {Label = "Benetton"}, new Seller {Label = "Benimolsun"}, new Seller {Label = "Bershka"},
                new Seller {Label = "Betashoes"}, new Seller {Label = "betashoes.com"}, new Seller {Label = "Beymen"},
                new Seller {Label = "Beymenclub"}, new Seller {Label = "Beyyoglu"}, new Seller {Label = "Bgstore"},
                new Seller {Label = "Bigdart"}, new Seller {Label = "Bilcee"}, new Seller {Label = "Bilge Kırıktaş"},
                new Seller {Label = "Bkmkitap"}, new Seller {Label = "Blacknoble"},
                new Seller {Label = "Bolesasuadiye"}, new Seller {Label = "Bondnon"}, new Seller {Label = "Bonprix"},
                new Seller {Label = "boutiquen.com.tr"}, new Seller {Label = "boyner"},
                new Seller {Label = "Brandroom"}, new Seller {Label = "brcfashion"}, new Seller {Label = "Browwauw"},
                new Seller {Label = "BSL"}, new Seller {Label = "Btmmoda"}, new Seller {Label = "Bulsakalsak"},
                new Seller {Label = "Bunubanayolla"}, new Seller {Label = "Burcutesettur"},
                new Seller {Label = "Butikbira"}, new Seller {Label = "Butikemine"}, new Seller {Label = "Butikeros"},
                new Seller {Label = "Butikmerveaksoy"}, new Seller {Label = "Butikvanilya"},
                new Seller {Label = "Buyukanneminsandigi"}, new Seller {Label = "Buyukbedenelbise"},
                new Seller {Label = "Buyukbedeniz"}, new Seller {Label = "Byselincigerci"},
                new Seller {Label = "Cabani"}, new Seller {Label = "cacharel"}, new Seller {Label = "Cantaponcik"},
                new Seller {Label = "Caponeoutfitters"}, new Seller {Label = "Cappmoda"}, new Seller {Label = "Carmen"},
                new Seller {Label = "Carpe"}, new Seller {Label = "Cartersoshkosh"}, new Seller {Label = "Cevherat"},
                new Seller {Label = "Ceyizdiyari"}, new Seller {Label = "Ceyizedairhersey"},
                new Seller {Label = "Chamakhbutik"}, new Seller {Label = "Chamakhbutik"}, new Seller {Label = "Chetic"},
                new Seller {Label = "Chima"}, new Seller {Label = "Ciceksepeti"}, new Seller {Label = "Cilekbutik"},
                new Seller {Label = "Cimcimebebe"}, new Seller {Label = "Civilim"}, new Seller {Label = "Clinique"},
                new Seller {Label = "COLİN`S"}, new Seller {Label = "Collezione"}, new Seller {Label = "Comertshoes"},
                new Seller {Label = "Corapfestivali"}, new Seller {Label = "Coraptoptancisi"},
                new Seller {Label = "Cottonmood"}, new Seller {Label = "dagi.com"}, new Seller {Label = "Damattween"},
                new Seller {Label = "Davidjones"}, new Seller {Label = "Decathlon"}, new Seller {Label = "Deery"},
                new Seller {Label = "DeFacto"}, new Seller {Label = "Delisiyim"}, new Seller {Label = "denizbutik"},
                new Seller {Label = "Deppoavantaj"}, new Seller {Label = "Deriden"}, new Seller {Label = "Derimod"},
                new Seller {Label = "Deripabuc"}, new Seller {Label = "Dermoeczanem"},
                new Seller {Label = "Dermokozmetika"}, new Seller {Label = "Desa"},
                new Seller {Label = "Devrekbastonlari"}, new Seller {Label = "dhgate"},
                new Seller {Label = "Dilarabutik"}, new Seller {Label = "Dilvin"}, new Seller {Label = "Dioxtea"},
                new Seller {Label = "Divarese"}, new Seller {Label = "Dolce-gusto"}, new Seller {Label = "dr.com.tr"},
                new Seller {Label = "Durununbutigi"}, new Seller {Label = "Dusunbilkitap"},
                new Seller {Label = "D`S damat"}, new Seller {Label = "E-bebek"}, new Seller {Label = "Eatalyshoes"},
                new Seller {Label = "Ebrumaternity"}, new Seller {Label = "Ecanta"},
                new Seller {Label = "Eceninbutigi"}, new Seller {Label = "Eczane"}, new Seller {Label = "Edataspinar"},
                new Seller {Label = "Efbutik"}, new Seller {Label = "Egzotikangels"}, new Seller {Label = "Ekolonline"},
                new Seller {Label = "Elelebaby"}, new Seller {Label = "Elfidatesettur"},
                new Seller {Label = "Elizamoda"}, new Seller {Label = "Elle Shoes"}, new Seller {Label = "Eminosbutik"},
                new Seller {Label = "Emsan"}, new Seller {Label = "Emsan"}, new Seller {Label = "Enbipharma"},
                new Seller {Label = "Englishhome"}, new Seller {Label = "Ervaninbutigi"},
                new Seller {Label = "Esrabastug"}, new Seller {Label = "eveshop"}, new Seller {Label = "Evyapshop"},
                new Seller {Label = "Existsilver"}, new Seller {Label = "Eyupsabrituncer"}, new Seller {Label = "Eyyo"},
                new Seller {Label = "Faiksonmez"}, new Seller {Label = "Faststep"}, new Seller {Label = "Fenerium"},
                new Seller {Label = "Fidankitap"}, new Seller {Label = "Fiseason"}, new Seller {Label = "Fitbas.com"},
                new Seller {Label = "FLO"}, new Seller {Label = "Foxayakkabi"}, new Seller {Label = "Fullamoda"},
                new Seller {Label = "GAP"}, new Seller {Label = "Gesmoda"}, new Seller {Label = "Gigis"},
                new Seller {Label = "gittigidiyor.com"}, new Seller {Label = "Gizyomoda"},
                new Seller {Label = "Goopac.store"}, new Seller {Label = "Gozlukcu"}, new Seller {Label = "Gratis"},
                new Seller {Label = "Greyder"}, new Seller {Label = "GSStore"},
                new Seller {Label = "Gulsahintakitezhahi"}, new Seller {Label = "Guneskitabevi"},
                new Seller {Label = "Gustoeshop"}, new Seller {Label = "H&M"}, new Seller {Label = "H&M Turkey"},
                new Seller {Label = "Hairtox"}, new Seller {Label = "Happy"}, new Seller {Label = "Hapshoe"},
                new Seller {Label = "Hasad"}, new Seller {Label = "Hataydanal"}, new Seller {Label = "Hatemoglu"},
                new Seller {Label = "Havos"}, new Seller {Label = "Haydigiy"}, new Seller {Label = "Hazelanna"},
                new Seller {Label = "hbbagstore"}, new Seller {Label = "Hediyemen"}, new Seller {Label = "Hepsiburada"},
                new Seller {Label = "Hobidunya"}, new Seller {Label = "Hollylolly"}, new Seller {Label = "Hotiç"},
                new Seller {Label = "Humabebe"}, new Seller {Label = "Hypetr"}, new Seller {Label = "İfondi"},
                new Seller {Label = "Ikea"}, new Seller {Label = "Ikoshoe"}, new Seller {Label = "İloveshoes"},
                new Seller {Label = "Ilvi"}, new Seller {Label = "İnna"}, new Seller {Label = "Instreet"},
                new Seller {Label = "Ipeksekerciler"}, new Seller {Label = "Ipekyol"},
                new Seller {Label = "Istasyongiyim"}, new Seller {Label = "Jammybaby"}, new Seller {Label = "Jimmykey"},
                new Seller {Label = "Journey"}, new Seller {Label = "Justinbeaty"}, new Seller {Label = "Kadinmodasi"},
                new Seller {Label = "Karaca-home"}, new Seller {Label = "Kartalyuvasi"}, new Seller {Label = "Kayra"},
                new Seller {Label = "kemal tanca"}, new Seller {Label = "Kigılı"}, new Seller {Label = "Kikiriki"},
                new Seller {Label = "Kip"}, new Seller {Label = "Kirmizionline"}, new Seller {Label = "Kitapsec"},
                new Seller {Label = "Kitapyurdu"}, new Seller {Label = "Kokopatik"}, new Seller {Label = "Kom"},
                new Seller {Label = "Kombincim"}, new Seller {Label = "Konudukkan"}, new Seller {Label = "Kostebek"},
                new Seller {Label = "KOTON"}, new Seller {Label = "koton.com"}, new Seller {Label = "Kozmela"},
                new Seller {Label = "Kozmobox"}, new Seller {Label = "Kshop"}, new Seller {Label = "Kubrabutik"},
                new Seller {Label = "Lafaba"}, new Seller {Label = "Laluvia"}, new Seller {Label = "Laminta"},
                new Seller {Label = "Laranor"}, new Seller {Label = "Lavinyaa"}, new Seller {Label = "LC  Waikiki"},
                new Seller {Label = "Lego.Storeturkey"}, new Seller {Label = "Lensbak"}, new Seller {Label = "Lescon"},
                new Seller {Label = "Lidyana"}, new Seller {Label = "Lilasxxl"}, new Seller {Label = "Limoya"},
                new Seller {Label = "Loft.com"}, new Seller {Label = "Lookadikoy"}, new Seller {Label = "Lovemybody"},
                new Seller {Label = "Loya"}, new Seller {Label = "LTBjeans"}, new Seller {Label = "Lufian"},
                new Seller {Label = "Lutvelizade"}, new Seller {Label = "Machka"},
                new Seller {Label = "Maciterkozmetik"}, new Seller {Label = "Madame Coco"},
                new Seller {Label = "MANGO"}, new Seller {Label = "Marjin"}, new Seller {Label = "Markafoni"},
                new Seller {Label = "Marksandspencer"}, new Seller {Label = "Maskbutik"},
                new Seller {Label = "massimodutti"}, new Seller {Label = "Matbutikshop"},
                new Seller {Label = "Matmazel"}, new Seller {Label = "Mavi.com"}, new Seller {Label = "Maymoda"},
                new Seller {Label = "MAYNATURA"}, new Seller {Label = "Mayox"}, new Seller {Label = "Mazibutik"},
                new Seller {Label = "Merinaturel"}, new Seller {Label = "Mervellion"},
                new Seller {Label = "Metkobutik"}, new Seller {Label = "Michael Kors.global"},
                new Seller {Label = "Milanoor"}, new Seller {Label = "Minetanbutik"}, new Seller {Label = "Minicity"},
                new Seller {Label = "Minikterzi"}, new Seller {Label = "Minipicco"}, new Seller {Label = "Miniso"},
                new Seller {Label = "Mispacoz"}, new Seller {Label = "Missha"}, new Seller {Label = "Moda1001"},
                new Seller {Label = "ModaALAbutik"}, new Seller {Label = "Modaayakkabi"},
                new Seller {Label = "Modacelikler"}, new Seller {Label = "Modailgi"},
                new Seller {Label = "Modakapimda"}, new Seller {Label = "Modakasmir"},
                new Seller {Label = "Modamizbir"}, new Seller {Label = "modanisa"},
                new Seller {Label = "Modasahtesettur"}, new Seller {Label = "Modaselvim"},
                new Seller {Label = "Modasena"}, new Seller {Label = "Modatrend"}, new Seller {Label = "Modavitrini"},
                new Seller {Label = "Monsoonlondon"}, new Seller {Label = "Mooibutik"}, new Seller {Label = "Morhipo"},
                new Seller {Label = "Mossta"}, new Seller {Label = "Mudo"}, new Seller {Label = "Mupashoes"},
                new Seller {Label = "mydukkan"}, new Seller {Label = "Mylovebutik"},
                new Seller {Label = "Mypoppishoes"}, new Seller {Label = "Mysilvers"}, new Seller {Label = "Myvitrinn"},
                new Seller {Label = "n11.com"}, new Seller {Label = "Nadirkitap"}, new Seller {Label = "Naiaistanbul"},
                new Seller {Label = "Naramaxx"}, new Seller {Label = "Narferita"}, new Seller {Label = "Neroxtea"},
                new Seller {Label = "Neslihancanpolat"}, new Seller {Label = "NetWork"},
                new Seller {Label = "Newbalance"}, new Seller {Label = "Newobsessions"},
                new Seller {Label = "Nextdirect"}, new Seller {Label = "Nike"}, new Seller {Label = "Ninewest"},
                new Seller {Label = "Nisantasishoes"}, new Seller {Label = "Nobelkitabevi"},
                new Seller {Label = "nocturne"}, new Seller {Label = "Nofilterbutik"}, new Seller {Label = "Nonabutik"},
                new Seller {Label = "Oblavion"}, new Seller {Label = "Occaasion"}, new Seller {Label = "Onespraytatoo"},
                new Seller {Label = "Onukgiyim"}, new Seller {Label = "Onzemode"}, new Seller {Label = "Oxxo"},
                new Seller {Label = "Oynasana"}, new Seller {Label = "Oysho"}, new Seller {Label = "Ozdilekteyim"},
                new Seller {Label = "Ozgeozgenc"}, new Seller {Label = "Paandabutik"}, new Seller {Label = "Panco"},
                new Seller {Label = "Pantopark"}, new Seller {Label = "patırtı"},
                new Seller {Label = "Payidarhomeconcept"}, new Seller {Label = "Pelininayakkabilari"},
                new Seller {Label = "Pelinsenoglu"}, new Seller {Label = "Penti"}, new Seller {Label = "Penyemood"},
                new Seller {Label = "Peraboutiqque"}, new Seller {Label = "Perspective"},
                new Seller {Label = "Pierre Cardin"}, new Seller {Label = "Pinarsboutique"},
                new Seller {Label = "Pixylove"}, new Seller {Label = "Podyumplus"}, new Seller {Label = "Popoptik"},
                new Seller {Label = "Poshet"}, new Seller {Label = "Pudrapayet"}, new Seller {Label = "Pullandbear"},
                new Seller {Label = "Puma"}, new Seller {Label = "Pumpsup"}, new Seller {Label = "Purplebybanu"},
                new Seller {Label = "Purpuravioleta"}, new Seller {Label = "Quzu"}, new Seller {Label = "Ramsey"},
                new Seller {Label = "Relaxmode"}, new Seller {Label = "Replikayakkabi"},
                new Seller {Label = "Retrobird"}, new Seller {Label = "Rhytmine"}, new Seller {Label = "Roman"},
                new Seller {Label = "Rossmann"}, new Seller {Label = "Saatevi"}, new Seller {Label = "Saatkordoncusu"},
                new Seller {Label = "Sabunique"}, new Seller {Label = "Sahibinden"}, new Seller {Label = "Saklibutik"},
                new Seller {Label = "Saklibutik"}, new Seller {Label = "Saklibutik"}, new Seller {Label = "Saklibutik"},
                new Seller {Label = "Sateen"}, new Seller {Label = "Saygigiyim"}, new Seller {Label = "Secilstore"},
                new Seller {Label = "Sedatutulmaz"}, new Seller {Label = "Sedayalcinatelier"},
                new Seller {Label = "Sefamerve"}, new Seller {Label = "Selectmoda"}, new Seller {Label = "Selinshoes"},
                new Seller {Label = "Seninfarkin"}, new Seller {Label = "Seninolsun"},
                new Seller {Label = "Sentetiksezar"}, new Seller {Label = "Sephora"}, new Seller {Label = "Serbella"},
                new Seller {Label = "Setre"}, new Seller {Label = "Shein.tr."}, new Seller {Label = "Sheloveayakkabi"},
                new Seller {Label = "Shemdi"}, new Seller {Label = "shoe tek"}, new Seller {Label = "Shoebutik"},
                new Seller {Label = "Shoetekfiyat"}, new Seller {Label = "shop.adidas"},
                new Seller {Label = "Shop.sarar"}, new Seller {Label = "Shopier"}, new Seller {Label = "Sibeldurmaz"},
                new Seller {Label = "Silesilver"}, new Seller {Label = "Simpleandchicco.dukkan"},
                new Seller {Label = "Sindimoda"}, new Seller {Label = "Sisistanbul"},
                new Seller {Label = "Siyahincionline"}, new Seller {Label = "Skechers"},
                new Seller {Label = "Socksstations"}, new Seller {Label = "Soflycosmetics"},
                new Seller {Label = "Sogutlusilver"}, new Seller {Label = "Sokakbutik"},
                new Seller {Label = "Solomombaby"}, new Seller {Label = "Soobe"}, new Seller {Label = "sportive.com"},
                new Seller {Label = "Spotbalk"}, new Seller {Label = "Stilask"}, new Seller {Label = "Stradivarius"},
                new Seller {Label = "Straswans"}, new Seller {Label = "Supermino"}, new Seller {Label = "Suvari"},
                new Seller {Label = "tarzkiz.com"}, new Seller {Label = "Tazekuru"}, new Seller {Label = "Tekstilev"},
                new Seller {Label = "Tergan"}, new Seller {Label = "Terrarossabutik"},
                new Seller {Label = "Tesetturdunyasi"}, new Seller {Label = "Tesetturpazari"},
                new Seller {Label = "Thebodyshop"}, new Seller {Label = "Tiamoda"}, new Seller {Label = "Tisho"},
                new Seller {Label = "Tisort.ist"}, new Seller {Label = "tofisa"}, new Seller {Label = "Tommylife"},
                new Seller {Label = "toyzzshop"}, new Seller {Label = "Trendbende"}, new Seller {Label = "Trendstore"},
                new Seller {Label = "Trendyes"}, new Seller {Label = "Trendyol"}, new Seller {Label = "Trikomisirli"},
                new Seller {Label = "Tubabutik"}, new Seller {Label = "Tuesbutik"}, new Seller {Label = "Tugbavenn"},
                new Seller {Label = "Tutyakala"}, new Seller {Label = "Tuvid"}, new Seller {Label = "Twist"},
                new Seller {Label = "U.S. POLO ASSN (TR)"}, new Seller {Label = "Ulkucumarket"},
                new Seller {Label = "Uludagtriko"}, new Seller {Label = "Upwatch"}, new Seller {Label = "Urbandecay"},
                new Seller {Label = "Urbanmuse"}, new Seller {Label = "Vekem"}, new Seller {Label = "Viadellerose"},
                new Seller {Label = "Vicco"}, new Seller {Label = "Vitmadogalgida"},
                new Seller {Label = "Vizonayakkabi"}, new Seller {Label = "Wahshe"}, new Seller {Label = "Watsons"},
                new Seller {Label = "Wcollection"}, new Seller {Label = "Wlabkozmetik"},
                new Seller {Label = "Yakestore"}, new Seller {Label = "Ydsshop"}, new Seller {Label = "Yer6store.com"},
                new Seller {Label = "Yvesrocher"}, new Seller {Label = "ZARA"}, new Seller {Label = "Zeqmoda"},
                new Seller {Label = "Zoombutik"}, new Seller {Label = ""}

            };
            return list;

        }
    }
}
