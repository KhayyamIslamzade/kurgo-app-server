﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class user_detail_column_type_changed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdentificationNumber",
                table: "UserDetails",
                maxLength: 11,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char",
                oldMaxLength: 11);

            migrationBuilder.AlterColumn<string>(
                name: "FinNumber",
                table: "UserDetails",
                maxLength: 7,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "char",
                oldMaxLength: 7);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IdentificationNumber",
                table: "UserDetails",
                type: "char",
                maxLength: 11,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 11);

            migrationBuilder.AlterColumn<string>(
                name: "FinNumber",
                table: "UserDetails",
                type: "char",
                maxLength: 7,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 7);
        }
    }
}
