﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class parcel_option_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parcels_Currencies_TransportPriceCurrencyId",
                table: "Parcels");

            migrationBuilder.DropIndex(
                name: "IX_Parcels_TransportPriceCurrencyId",
                table: "Parcels");

            migrationBuilder.DropColumn(
                name: "TransportPrice",
                table: "Parcels");

            migrationBuilder.DropColumn(
                name: "TransportPriceCurrencyId",
                table: "Parcels");

            migrationBuilder.DropColumn(
                name: "Weight",
                table: "Parcels");

            migrationBuilder.CreateTable(
                name: "ParcelOptions",
                columns: table => new
                {
                    ParcelId = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: true),
                    DateDeleted = table.Column<DateTime>(nullable: true),
                    Status = table.Column<byte>(nullable: false),
                    TransportPrice = table.Column<double>(nullable: false),
                    Weight = table.Column<double>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParcelOptions", x => x.ParcelId);
                    table.ForeignKey(
                        name: "FK_ParcelOptions_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ParcelOptions_Parcels_ParcelId",
                        column: x => x.ParcelId,
                        principalTable: "Parcels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ParcelOptions_CurrencyId",
                table: "ParcelOptions",
                column: "CurrencyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ParcelOptions");

            migrationBuilder.AddColumn<double>(
                name: "TransportPrice",
                table: "Parcels",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "TransportPriceCurrencyId",
                table: "Parcels",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Weight",
                table: "Parcels",
                type: "double precision",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateIndex(
                name: "IX_Parcels_TransportPriceCurrencyId",
                table: "Parcels",
                column: "TransportPriceCurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parcels_Currencies_TransportPriceCurrencyId",
                table: "Parcels",
                column: "TransportPriceCurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
