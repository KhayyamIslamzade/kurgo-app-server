﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class parcel_transport_price_currency_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TransportPriceCurrencyId",
                table: "Parcels",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Parcels_TransportPriceCurrencyId",
                table: "Parcels",
                column: "TransportPriceCurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Parcels_Currencies_TransportPriceCurrencyId",
                table: "Parcels",
                column: "TransportPriceCurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Parcels_Currencies_TransportPriceCurrencyId",
                table: "Parcels");

            migrationBuilder.DropIndex(
                name: "IX_Parcels_TransportPriceCurrencyId",
                table: "Parcels");

            migrationBuilder.DropColumn(
                name: "TransportPriceCurrencyId",
                table: "Parcels");
        }
    }
}
