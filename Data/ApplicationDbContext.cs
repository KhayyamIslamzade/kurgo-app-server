﻿using Core.Constants;
using Data.Configurations;
using Data.Entities;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Data
{

    public class ApplicationDbContext : IdentityDbContext<User,
        Role, string, UserClaim,
        UserRole,
        UserLogin,
        RoleClaim,
        UserToken>

    {

        #region Identity
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<PermissionCategory> PermissionCategories { get; set; }
        public DbSet<PermissionCategoryPermission> PermissionCategoryPermissions { get; set; }
        public DbSet<RolePermissionCategory> RolePermissionCategories { get; set; }
        public DbSet<EmailConfirmationRequest> EmailConfirmationRequests { get; set; }
        public DbSet<PasswordResetRequest> PasswordResetRequests { get; set; }

        #endregion
        public DbSet<Parcel> Parcels { get; set; }
        public DbSet<ParcelOption> ParcelOptions { get; set; }
        public DbSet<ParcelStatus> ParcelStatuses { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<UserDetail> UserDetails { get; set; }




        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new RolePermissionCategoryConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserPermissionConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());

            modelBuilder.Entity<User>().ToTable("Users", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<Role>().ToTable("Roles", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<UserRole>().ToTable("UserRoles", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<UserToken>().ToTable("UserTokens", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<RoleClaim>().ToTable("RoleClaims", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<RolePermissionCategory>().ToTable("RolePermissionCategories", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<UserPermission>().ToTable("UserPermissions", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<Permission>().ToTable("Permissions", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<PermissionCategory>().ToTable("PermissionCategories", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<PermissionCategoryPermission>().ToTable("PermissionCategoryPermissions", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<EmailConfirmationRequest>().ToTable("EmailConfirmationRequests", GeneralConstants.IdentityTableSchemeName);
            modelBuilder.Entity<PasswordResetRequest>().ToTable("PasswordResetRequests", GeneralConstants.IdentityTableSchemeName);

            modelBuilder.Entity<Parcel>().ToTable("Parcels");
            modelBuilder.Entity<ParcelOption>().ToTable("ParcelOptions");
            modelBuilder.Entity<ParcelStatus>().ToTable("ParcelStatuses");
            modelBuilder.Entity<Category>().ToTable("Categories");
            modelBuilder.Entity<Seller>().ToTable("Sellers");
            modelBuilder.Entity<Store>().ToTable("Stores");
            modelBuilder.Entity<Currency>().ToTable("Currencies");
            modelBuilder.Entity<UserDetail>().ToTable("UserDetails");
            modelBuilder.SetStatusQueryFilter();

        }
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries())
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["DateCreated"] = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.CurrentValues["DateModified"] = DateTime.Now;
                        break;
                }
        }
    }
}
