﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Currency : HelperModel, IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Label { get; set; }
        [StringLength(GeneralConstants.LengthLg)]
        public string Description { get; set; }
        public ICollection<Parcel> Parcels { get; set; } = new Collection<Parcel>();
        public ICollection<ParcelOption> ParcelOptions { get; set; } = new Collection<ParcelOption>();

    }
}
