﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using Data.Entities.Identity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Data.Entities
{
    //transport product from one location to other
    public class Parcel : HelperModel, IEntity
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        [Required]
        public string UserId { get; set; }
        public User User { get; set; }

        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string TrackingNumber { get; set; }


        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        [ForeignKey("Currency")]
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }

        [ForeignKey("Seller")]
        public int SellerId { get; set; }
        public Seller Seller { get; set; }
        public ParcelOption ParcelOption { get; set; }
        public int Quantity { get; set; }
        public double ProductPrice { get; set; }

        [StringLength(GeneralConstants.LengthLg)]
        public string Description { get; set; }
        public ICollection<ParcelStatus> ParcelStatuses { get; set; } = new Collection<ParcelStatus>();

        [NotMapped]
        public ParcelStatus ParcelStatus => ParcelStatuses.OrderByDescending(c => c.DateCreated).FirstOrDefault();
    }
}
