﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Store : HelperModel, IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Country { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string City { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthMd)]
        public string Adress { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxxs)]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxxs)]
        public string ZipCode { get; set; }


    }
}
