﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities.Identity
{
    public class PermissionCategory : HelperModel, IEntity
    {
        [Key]
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Label { get; set; }
        [StringLength(GeneralConstants.LengthSm)]
        public string VisibleLabel { get; set; }
        [StringLength(GeneralConstants.LengthMd)]
        public string Description { get; set; }

        public ICollection<PermissionCategoryPermission> PossiblePermissions { get; set; } = new Collection<PermissionCategoryPermission>();

    }
}
