﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities.Identity
{
    public class EmailConfirmationRequest : HelperModel, IEntity
    {
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXs)]
        public string UserId { get; set; }
        public User User { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}
