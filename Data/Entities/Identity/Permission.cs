﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities.Identity
{
    public class Permission : HelperModel, IEntity
    {
        [Key]
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Label { get; set; }

        [StringLength(GeneralConstants.LengthSm)]
        public string VisibleLabel { get; set; }

        [StringLength(GeneralConstants.LengthMd)]
        public string Description { get; set; }
        public bool IsDirective { get; set; }

        public ICollection<PermissionCategoryPermission> Categories { get; set; }
        public Permission() { Categories = new Collection<PermissionCategoryPermission>(); }
    }
}
