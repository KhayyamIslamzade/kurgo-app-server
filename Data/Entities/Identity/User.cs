﻿using Core.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Data.Entities.Identity
{

    public class User : IdentityUser<string>, IEntity, IHelperModel
    {

        public ICollection<UserRole> Roles { get; set; } = new Collection<UserRole>();
        public ICollection<UserPermission> DirectivePermissions { get; set; } = new Collection<UserPermission>();
        public ICollection<Parcel> Parcels { get; set; } = new Collection<Parcel>();
        public ICollection<EmailConfirmationRequest> EmailConfirmationRequests { get; set; } = new Collection<EmailConfirmationRequest>();
        public ICollection<PasswordResetRequest> PasswordResetRequests { get; set; } = new Collection<PasswordResetRequest>();
        public UserDetail Detail { get; set; }
        public bool IsEditable { get; set; } = true;
        public bool IsDetailCompleted { get; set; } = false;

        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public byte Status { get; set; }
    }
}
