﻿using Core.BaseModels;
using Core.Constants;
using Core.Enums;
using Core.Interfaces;
using Data.Entities.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class UserDetail : HelperModel, IEntity
    {

        [Key]
        [ForeignKey("User")]
        [Required]
        public string UserId { get; set; }
        public User User { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxs)]
        public string Name { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxs)]
        public string Surname { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthMd)]
        public string Adress { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthXxxs)]
        public string PhoneNumber { get; set; }

        public byte Gender { get; set; } = (byte)GenderEnum.Male;
        public DateTime Birthday { get; set; }
        [Required]
        [StringLength(11)]//fixed length
        public string IdentificationNumber { get; set; }
        [Required]
        [StringLength(7)]//fixed length
        public string FinNumber { get; set; }

    }
}
