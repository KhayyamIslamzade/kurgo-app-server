﻿using Core.BaseModels;
using Core.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class ParcelOption : HelperModel, IEntity
    {

        [Key]
        [ForeignKey("Parcel")]
        [Required]
        public int ParcelId { get; set; }
        public double TransportPrice { get; set; }
        public double Weight { get; set; }

        [ForeignKey("Currency")]
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
    }
}
