﻿using Core.BaseModels;
using Core.Constants;
using Core.Enums;
using Core.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class ParcelStatus : HelperModel, IEntity
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Parcel")]
        public int ParcelId { get; set; }
        public Parcel Parcel { get; set; }
        public byte StatusOfParcel { get; set; } = (byte)ParcelStatusEnum.OnProcess;
        [StringLength(GeneralConstants.LengthLg)]
        public string Description { get; set; }


    }
}
