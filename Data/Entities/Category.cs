﻿using Core.BaseModels;
using Core.Constants;
using Core.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Category : HelperModel, IEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(GeneralConstants.LengthSm)]
        public string Label { get; set; }
        [StringLength(GeneralConstants.LengthLg)]
        public string Description { get; set; }
        private ICollection<Parcel> Parcels { get; set; } = new Collection<Parcel>();
    }
}
