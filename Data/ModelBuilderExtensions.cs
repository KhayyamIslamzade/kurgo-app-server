﻿using Core.Enums;
using Data.Entities;
using Data.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace Data
{
    public static class ModelBuilderExtensions
    {
        public static ModelBuilder SetStatusQueryFilter(this ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<MODEL>().HasQueryFilter(p => p.Status != (byte)RecordStatus.Deleted);

            modelBuilder.Entity<Parcel>().HasQueryFilter(p => p.Status != (byte)RecordStatus.Deleted);
            modelBuilder.Entity<Currency>().HasQueryFilter(p => p.Status != (byte)RecordStatus.Deleted);
            modelBuilder.Entity<Store>().HasQueryFilter(p => p.Status != (byte)RecordStatus.Deleted);
            modelBuilder.Entity<Category>().HasQueryFilter(p => p.Status != (byte)RecordStatus.Deleted);
            modelBuilder.Entity<Seller>().HasQueryFilter(p => p.Status != (byte)RecordStatus.Deleted);
            modelBuilder.Entity<EmailConfirmationRequest>().HasQueryFilter(p => p.ExpireDate >= DateTime.Now);


            return modelBuilder;
        }
    }
}
