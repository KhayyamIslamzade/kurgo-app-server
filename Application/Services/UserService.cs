﻿using Castle.Core.Internal;
using Core.Constants;
using Core.Enums;
using Data.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Process;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Application.Services
{
    public class UserService
    {
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(UserManager<User> userManager, IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetAuthorizedUserId()
        {
            return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public Task<User> GetUserByNameAsync(string userName)
        {
            if (userName.IsNullOrEmpty())
                return Task.FromResult<User>(null);
            return _userManager.FindByNameAsync(userName);
        }
        public Task<User> GetUserByNameAsync(string userName, params Expression<Func<User, object>>[] includeProperties)
        {
            if (userName.IsNullOrEmpty())
                return Task.FromResult<User>(null);
            var query = _userManager.Users.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.UserName == userName);
        }
        public Task<User> GetUserByNameAsync(string userName, params string[] includeProperties)
        {
            if (userName.IsNullOrEmpty())
                return Task.FromResult<User>(null);
            var query = _userManager.Users.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.UserName == userName);
        }

        public Task<User> GetUserByIdAsync(string userId)
        {
            return _userManager.FindByIdAsync(userId);
        }

        public Task<User> GetUserByIdAsync(string userId, params Expression<Func<User, object>>[] includeProperties)
        {
            var query = _userManager.Users.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.Id == userId);
        }


        public Task<User> GetUserByIdAsync(string userId, params string[] includeProperties)
        {
            var query = _userManager.Users.IncludeAll(includeProperties);

            return query.FirstOrDefaultAsync(c => c.Id == userId);
        }
        public Task<User> GetUserByEmailAsync(string email)
        {
            if (email.IsNullOrEmpty())
                return Task.FromResult<User>(null);
            return _userManager.FindByEmailAsync(email);
        }
        public IQueryable<User> FindBy(Expression<Func<User, bool>> predicate)
        {
            return _userManager.Users.Where(predicate);
        }
        public IQueryable<User> FindBy(Expression<Func<User, bool>> predicate, params string[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties).Where(predicate);
        }
        public IQueryable<User> FindBy(Expression<Func<User, bool>> predicate, params Expression<Func<User, object>>[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties).Where(predicate);
        }
        public IQueryable<User> GetAll()
        {
            return _userManager.Users;
        }
        public IQueryable<User> GetAll(params string[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties);
        }
        public IQueryable<User> GetAll(params Expression<Func<User, object>>[] includeProperties)
        {
            return _userManager.Users.IncludeAll(includeProperties);
        }



        public Task<bool> IsExistAsync(Expression<Func<User, bool>> predicate)
        {
            return _userManager.Users.AnyAsync(predicate);
        }
        public async Task<IdentityResult> ConfirmEmailAsync(string userId, string token)
        {
            //var decodedCode = HttpUtility.UrlDecode(token);
            var user = await GetUserByIdAsync(userId, c => c.EmailConfirmationRequests);

            var result = await _userManager.ConfirmEmailAsync(user, token);
            return result;

        }
        public async Task<string> CreateEmailConfirmationTokenAsync(string userId)
        {
            var user = await GetUserByIdAsync(userId, c => c.EmailConfirmationRequests);

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user).ConfigureAwait(false);
            var encodedToken = HttpUtility.UrlEncode(token);
            var lastRequest = user.EmailConfirmationRequests.LastOrDefault();
            if (lastRequest != null)
                lastRequest.Status = (byte)RecordStatus.Deleted;
            user.EmailConfirmationRequests.Add(new EmailConfirmationRequest
            {
                Token = encodedToken,
                ExpireDate = DateTime.Now.AddDays(1),

            });
            await UpdateAsync(user).ConfigureAwait(false);

            return encodedToken;
        }
        public async Task<IdentityResult> ConfirmPasswordResetTokenAsync(string userId, string token)
        {

            var user = await GetUserByIdAsync(userId, c => c.PasswordResetRequests);

            var result = await _userManager.ConfirmEmailAsync(user, token);
            return result;

        }
        public async Task<string> CreatePasswordResetTokenAsync(string userId)
        {
            var user = await GetUserByIdAsync(userId, c => c.PasswordResetRequests);

            var token = await _userManager.GeneratePasswordResetTokenAsync(user).ConfigureAwait(false);
            var encodedToken = HttpUtility.UrlEncode(token);
            var lastRequest = user.PasswordResetRequests.LastOrDefault();
            if (lastRequest != null)
                lastRequest.Status = (byte)RecordStatus.Deleted;
            user.PasswordResetRequests.Add(new PasswordResetRequest()
            {
                Token = encodedToken,
                ExpireDate = DateTime.Now.AddDays(1),

            });
            await UpdateAsync(user).ConfigureAwait(false);

            return encodedToken;
        }
        public Task<IdentityResult> CreateAsync(User user, string password)
        {
            return _userManager.CreateAsync(user, password);
        }
        public Task<IdentityResult> UpdateAsync(User user)
        {
            return _userManager.UpdateAsync(user);
        }
        public Task<IdentityResult> ChangePasswordAsync(User user, string oldPassword, string newPassword)
        {
            return _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }
        public async Task<IdentityResult> ResetPasswordAsync(string userId, string token, string newPassword)
        {
            var user = await GetUserByIdAsync(userId).ConfigureAwait(false);
            return await _userManager.ResetPasswordAsync(user, token, newPassword).ConfigureAwait(false);
        }
        public Task<IdentityResult> DeleteAsync(User user)
        {
            return _userManager.DeleteAsync(user);
        }

        public bool IsAdmin(User user)
        {
            return UserIsInPermission(user, nameof(PermissionEnum.Admin));
        }
        public async Task<bool> IsAdminAsync()
        {
            var userId = GetAuthorizedUserId();
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            return UserIsInPermission(await GetUserByIdAsync(userId, includeParams.ToArray()), nameof(PermissionEnum.Admin));
        }
        public async Task<bool> IsAdminAsync(string userId)

        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            return UserIsInPermission(await GetUserByIdAsync(userId, includeParams.ToArray()), nameof(PermissionEnum.Admin));
        }
        public async Task<bool> UserIsInRoleAsync(User user, string roleName)
        {
            var roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            return roles.Any(s => s.Equals(roleName, StringComparison.OrdinalIgnoreCase));
        }

        public bool UserIsInPermission(User user, string permissionName)
        {
            var directivePermissions = user.DirectivePermissions.Select(c => c.Permission.Label).ToList();
            var userRole = user.Roles.Select(c => c.Role).ToList();
            var permissions = userRole.SelectMany(c => c.PermissionCategory.Select(e => e.PermissionCategoryPermission.Permission.Label)).ToList();

            return directivePermissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase)) ||
                   permissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase));


        }
        public async Task<bool> UserIsInPermissionAsync(string userId, string permissionName)
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var user = await GetUserByIdAsync(userId, includeParams.ToArray()).ConfigureAwait(false);
            var directivePermissions = user.DirectivePermissions.Select(c => c.Permission.Label).ToList();
            var userRole = user.Roles.Select(c => c.Role).ToList();
            var permissions = userRole.SelectMany(c => c.PermissionCategory.Select(e => $"{ e.PermissionCategoryPermission.Category.Label.ToLower()}_{ e.PermissionCategoryPermission.Permission.Label.ToLower()}")).ToList();

            return directivePermissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase)) ||
                   permissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase));
        }



    }
}
