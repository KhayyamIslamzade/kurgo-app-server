﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Shared;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ConfirmationService
    {
        private readonly TokenService _tokenService;
        private readonly EmailService _emailService;
        private readonly UserService _userService;
        private readonly DomainSettings _appSettings;
        private readonly IWebHostEnvironment _environment;

        public ConfirmationService(TokenService tokenService, EmailService emailService, UserService userService, IOptions<DomainSettings> appSettings, IWebHostEnvironment environment)
        {
            _tokenService = tokenService;
            _emailService = emailService;
            _userService = userService;
            _environment = environment;
            _appSettings = appSettings.Value;
        }

        public async Task SendConfirmationMailAsync(string userId, string userEmail)
        {
            var token = await _userService.CreateEmailConfirmationTokenAsync(userId);

            var contentRoot = _environment.ContentRootPath;
            var emailTemplatePath = contentRoot + "\\File\\emailConfirmationTemplate.html";

            var forwardLink = $"{_appSettings.DomainAdress}/#/emailConfirmation?userId={userId}&token={token}";
            var content = System.IO.File.ReadAllText(emailTemplatePath)
                .Replace("@forwardLink", forwardLink);

            await _emailService.SendEmailAsync(new MailModel()
            {
                TargetMail = userEmail,
                Subject = "Please Confirm Your Email",
                Content = content
            });
        }

        public async Task SendPasswordResetMailAsync(string userId, string userEmail)
        {
            var token = await _userService.CreatePasswordResetTokenAsync(userId);

            var contentRoot = _environment.ContentRootPath;
            var emailTemplatePath = contentRoot + "\\File\\passwordResetTemplate.html";

            var forwardLink = $"{_appSettings.DomainAdress}/#/passwordReset?userId={userId}&token={token}";
            var content = System.IO.File.ReadAllText(emailTemplatePath)
                .Replace("@forwardLink", forwardLink);

            await _emailService.SendEmailAsync(new MailModel()
            {
                TargetMail = userEmail,
                Subject = "Password Reset",
                Content = content
            });
        }
    }
}
