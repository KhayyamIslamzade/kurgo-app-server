﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Application.Services
{
    public class TokenService
    {
        private readonly TokenSettings _tokenSettings;
        public TokenService(IOptions<TokenSettings> tokenSettings)
        {
            _tokenSettings = tokenSettings.Value;
        }

        public string GenerateToken(string userId)
        {

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString().Substring(0,16)),
                new Claim(ClaimTypes.NameIdentifier, userId),
            };

            //var appSettings = _configuration.GetSection("TokenSettings").Get<TokenSettings>();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenSettings.JwtKey));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expires = DateTime.Now.AddDays(Convert.ToDouble(_tokenSettings.JwtExpireDays));

            var token = new JwtSecurityToken(_tokenSettings.JwtIssuer, _tokenSettings.JwtIssuer, claims, expires: expires,
                signingCredentials: credentials);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
        public string GenerateToken(string userId, DateTime expires)
        {

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString().Substring(0,16)),
                new Claim(ClaimTypes.NameIdentifier, userId),
            };

            //var appSettings = _configuration.GetSection("TokenSettings").Get<TokenSettings>();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenSettings.JwtKey));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_tokenSettings.JwtIssuer, _tokenSettings.JwtIssuer, claims, expires: expires,
                signingCredentials: credentials);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenString = tokenHandler.WriteToken(token);


            return tokenString;
        }
        public bool ValidateCurrentToken(string token)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenSettings.JwtKey));

            var myIssuer = _tokenSettings.JwtIssuer;

            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = myIssuer,
                    ValidAudience = myIssuer,
                    IssuerSigningKey = key
                }, out SecurityToken validatedToken);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
