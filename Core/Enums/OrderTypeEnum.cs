﻿namespace Core.Enums
{
    public enum OrderTypeEnum : byte
    {

        JustTransport = 1,
        BuyForMe,
    }
}
