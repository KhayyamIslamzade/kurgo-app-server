﻿namespace Core.Enums
{
    public enum ParcelStatusEnum : byte
    {

        OnProcess = 1,
        WaitingApprove,
        Canceled,
        Transporting,
        Delivered,
        InOffice,

    }
}
