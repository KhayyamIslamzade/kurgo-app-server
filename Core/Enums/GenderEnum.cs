﻿namespace Core.Enums
{
    public enum GenderEnum : byte
    {

        Male = 1,
        Female,
        Other,
    }
}
